function UNIT_TEST_IS_OUTPUT_SAME(varargin)

[original_data_dir, new_data_dir, file_type] = getDirs(varargin);

[original_data, new_data] = getData(original_data_dir, new_data_dir, file_type);

if isequal(original_data, new_data)
    fprintf('Original data is unaffected by new changes\n:-)\n')
else
    fprintf('Original and new data are different\n:-O\n')
end

end

function [original_data_dir, new_data_dir, file_type] = getDirs(inputs)

dot_idx = find(inputs{2} == '.');
file_type = inputs{2}(dot_idx+1:end);

if length(inputs) < 3
    original_data_dir = fullfile([pwd, '\UNIT_TESTS\OriginalData\', inputs{2}]);
    new_data_dir = fullfile([inputs{1}, '\', inputs{2}]);
else
    original_data_dir = fullfile([pwd, '\UNIT_TESTS\OriginalData\', inputs{2}]);
    if isempty(original_data_dir)
        original_data_dir = fullfile([pwd, '\UNIT_TESTS\OriginalData\', inputs{3}]);
        new_data_dir = fullfile([inputs{1}, '\', inputs{2}]);
    else
        new_data_dir = fullfile([inputs{1}, '\', inputs{3}]);
    end
    
end

if isempty(original_data_dir)
    error('Original data DNE!!!')
end

end

function [original_data, new_data] = getData(original_data_dir, new_data_dir, file_type)

rounding_tol = 5;

if contains(file_type, 'gcode')
    FID1 = fopen(original_data_dir);
    original_data = textscan(FID1,'%s','Delimiter','');
    fclose(FID1);
    original_data = original_data{1};
    FID2 = fopen(new_data_dir);
    new_data = textscan(FID2,'%s','Delimiter','');
    fclose(FID2);
    new_data = new_data{1};
elseif contains(file_type, 'mat')
    original_data = round(load(original_data_dir), rounding_tol);
    new_data = round(load(new_data_dir), rounding_tol);
else
    %NOTE: Currently assumes this will be a numeric array (ex. not mixed 
    %type or string text files)
    original_data = round(readmatrix(original_data_dir), rounding_tol);
    new_data = round(readmatrix(new_data_dir), rounding_tol);
end

end