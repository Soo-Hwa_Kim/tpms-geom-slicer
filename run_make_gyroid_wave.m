num_periods = 2;

%stuff to always run
height = num_periods * (2*pi); %x max
width = height; %y max

x = linspace(0, height, 100)';

delz = 0.02;
Z = 0:delz:height;
% Z = linspace(0, height, 100);

%% 2D plots
x_all = [];
y_all = [];
z_all = [];
figure(1)
for z = Z
%         if z == 0.6
%         keyboard
%     end
    vertical = abs(sin(z)) <= abs(cos(z));
    points = make_gyroid_wave_alt(z, x, height, width, true);
    clf
    xlim([0, height])
    ylim([0, width])
    hold all
    for n = 1:size(points, 1)
        plot(points(n,:,2), points(n,:,1), 'r-', 'LineWidth',2)
        title(sprintf('Z = %1.2f',z))
%         if vertical
%             title('vertical')
%         else
%             title('not vertical')
%         end
    end
%     for n = 1:size(points, 1)/2
%         plot(points(2*n-1,:,1), points(2*n-1,:,2), '.')
%     end
    pause(.1)

    x_all = [x_all; points(:,:,1)];
    y_all = [y_all; points(:,:,2)];
    z_all = [z_all; z*ones(size(points(:,:,1)))];
    if z == 0.6
        keyboard
    end

end

xyz = [x_all(:), y_all(:), z_all(:)];
prev_xyz = load('xyz.mat');

prev_xyz = prev_xyz.xyz;

%% 3D Plot with isosurface
isosurface_inputs = isosurfaceGyroid;
[faces, vertices] = isosurface(isosurface_inputs{:});
figure
scatter3(x_all(:), y_all(:), z_all(:), 'filled'); hold on
scatter3(vertices(:,2),vertices(:,1),vertices(:,3),20, 'filled','MarkerFaceColor','b','MarkerEdgeColor','b',...
    'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
xlim([0,2*pi])
ylim([0,2*pi])
zlim([0,2*pi])
xlabel('x')
ylabel('y')
zlabel('z')
legend('mygyroid','isosurface')
%
figure
scatter3(x_all(:), y_all(:), z_all(:), 20,'filled','MarkerFaceColor','b','MarkerEdgeColor','b',...
    'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
xlim([0,height])
ylim([0,height])
zlim([0,height])
xlabel('x')
ylabel('y')
zlabel('z')
title('run make gyroid wave')
%
% figure
% isosurface(isosurface_inputs{:})
% xlim([0,2*pi])
% ylim([0,2*pi])
% zlim([0,2*pi])
% xlabel('x')
% ylabel('y')
% zlabel('z')

