close all
num_periods = 1;
height_mm = num_periods * (2*pi);
length_mm = num_periods * (2*pi);
finputs = {num_periods, length_mm};

layer_height = 0.2;
num_layers = height_mm/layer_height;
Z = layer_height*(1:num_layers);
% Z = [0 Z];

x_all = [];
y_all = [];
z_all = [];
figure(1)
for n = 1:num_layers
    
    z_mm = Z(n);
%     if z_mm==.78
%         keyboard
%     end
    vertical = abs(sin(z_mm)) <= abs(cos(z_mm));
    lines = make_gyroid_infill(num_periods, length_mm, z_mm, 100);
%     x = linspace(0, height, 100)';
%     y = generalFormulaGDI(x, z_mm*ones(size(x)), 'gyroid');
    clf
    xlim([100, 100+height])
    ylim([100, 100+width])
    hold all
%     for m = 1:size(lines, 1)
%         plot(lines(m,:,1), lines(m,:,2))
%         if vertical
%             title('vertical')
%         else
%             title('not vertical')
%         end
%     end
    pause(0.1)
    try
        x_all = [x_all; reshape(lines(:,:,1),[],100)];
        y_all = [y_all; reshape(lines(:,:,2),[],100)];
        z_all = [z_all; z_mm*ones(size(reshape(lines(:,:,1),[],100)))];
    catch
        keyboard
    end
end

xyz = [x_all(:)-100, y_all(:)-100, z_all(:)-100];
save('xyz','xyz');

figure(2)
scatter3(x_all(:)-100, y_all(:)-100, z_all(:), 20,'filled','MarkerFaceColor','b','MarkerEdgeColor','b',...
    'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
xlim([0,height_mm])
ylim([0,height_mm])
zlim([0,height_mm])
xlabel('x')
ylabel('y')
zlabel('z')
title('run make gyroid infill')
