function [parsed_file, perim_mean_w_f, extperim_f] = findDistances(parsed_file)

fields = fieldnames(parsed_file);
perim_mean_w_f = [];
extperim_f = [];
for n = 1:length(fields)
    
    if contains(fields{n}, 'LAYER')
        subfields = fieldnames(parsed_file.(fields{n}));
        for m = 1:length(subfields)
            if ~contains(subfields{m},'z') && ~contains(subfields{m},'height') && ~contains(subfields{m},'init_')
                xyzef = parsed_file.(fields{n}).(subfields{m}).XYZEF;
                xy = xyzef(find(xyzef(:,1)~=0), :);
                xy_no_extrude_idx = find(xy(:,4)==0);
                xy_extrude_idx = find(xy(:,4)~=0);
                dist_idx = 1;
                first_flag = true;
                for p = 1:length(xy)
                    if first_flag
                        if ~isempty(xy(p,4))
                            prev_x = xy(p,1);
                            prev_y = xy(p,2);
                            prev_e = xy(p,4);
                            first_flag = false;
                        end
                    else
                        if any(ismember(xy_extrude_idx, p))
                            curr_x = xy(p,1);
                            curr_y = xy(p,2);
                            curr_e = xy(p,4);
                            diff_x = curr_x - prev_x;
                            diff_y = curr_y - prev_y;
                            diff_e = curr_e;
                            distance = sqrt(diff_x.^2 + diff_y.^2);
                            dist_mat(dist_idx, :) = [distance, diff_e, diff_e./distance];
                            prev_x = curr_x;
                            prev_y = curr_y;
                            prev_e = curr_e;
                            dist_idx = dist_idx + 1;
                        elseif any(ismember(xy_no_extrude_idx, p))
                            prev_x = xy(p,1);
                            prev_y = xy(p,2);
                        end
                    end
                end
                pos_dist_mat = dist_mat(:,3)>0;
                avg_e_dist = mean(dist_mat(pos_dist_mat,3));
                parsed_file.(fields{n}).(subfields{m}).EXTRUDE_DIST.all_ext_dist = dist_mat;
                parsed_file.(fields{n}).(subfields{m}).EXTRUDE_DIST.avg_ext_per_dist = avg_e_dist;
                if contains(subfields{m}, 'Perimeter')
                    perim_mean_w_f = [perim_mean_w_f;avg_e_dist xyzef(1,5)];
                elseif contains(subfields{m}, 'External')
                    extperim_f = [extperim_f; xyzef(1,5)];
                end
                clear dist_mat
                
                
            end
        end
        
    end
    
end