function parsed_file = parseGCode(file_name)

FID = fopen(file_name);
data = textscan(FID,'%s','Delimiter','');
fclose(FID);
data = data{1};


idx_init = 1:min(find(contains(data, ';LAYER_CHANGE')))-1;

idx_ending = max(find(contains(data, ';TYPE:Custom'))):max(find(contains(data, 'M84')));

parsed_file = struct();
parsed_file.INIT = data(idx_init);
parsed_file.END = data(idx_ending);

layer_counter = 0;
fan_on_counter = 0;
fan_off_counter = 0;
for n_idx = max(idx_init)+1:min(idx_ending)-1
% for n_idx = 1:length(data)

    data_n = data{n_idx};
    
    if contains(data_n, ';LAYER_CHANGE') % layer # we are on
        layer_counter = layer_counter + 1;
        layer_label = ['LAYER', num2str(layer_counter)];
        type = 'init_layer_instruc';
        xyzef_idx = 1;
        misc_idx = 1;
        
    elseif contains(data_n, ';Z:') % Z height from bottom
        parsed_file.(layer_label).z = data_n;
        
    elseif contains(data_n, ';HEIGHT:') % height of current layer
        parsed_file.(layer_label).height = data_n;
        
    elseif contains(data_n, ';TYPE:') % type of print within layer
        type = data_n(7:end);
        type = type(find(~isspace(type)));
        xyzef_idx = 1;
        
    elseif contains(data_n, 'G1') % move x,y,z,e,f instructions
        x_idx = find(data_n == 'X');
        y_idx = find(data_n == 'Y');
        z_idx = find(data_n == 'Z');
        e_idx = find(data_n == 'E');
        f_idx = find(data_n == 'F');
        other_idx = find(data_n == ';');
        
        if ~isempty(x_idx)
            z = 0;
            x = str2num(data_n(x_idx+1:y_idx-1));
            if ~isempty(e_idx)
                y = str2num(data_n(y_idx+1:e_idx-1));
                f = 0;
                e = str2num(data_n(e_idx+1:end));
            elseif ~isempty(f_idx)
                y = str2num(data_n(y_idx+1:f_idx-1));
                e = 0;
                if isempty(other_idx)
                    f = str2num(data_n(f_idx+1:end));
                else
                    f = str2num(data_n(f_idx+1:other_idx-1));
                end
            else
                y = str2num(data_n(y_idx+1:end));
                e = 0;
                f = 0;
            end
            
        elseif ~isempty(z_idx)
            x = 0;
            y = 0;
            if ~isempty(e_idx)
                z = str2num(data_n(z_idx+1:e_idx-1));
                f = 0;
                e = str2num(data_n(e_idx+1:end));
            elseif ~isempty(f_idx)
                z = str2num(data_n(z_idx+1:f_idx-1));
                e = 0;
                if isempty(other_idx)
                    f = str2num(data_n(f_idx+1:end));
                else
                    f = str2num(data_n(f_idx+1:other_idx-1));
                end
            else
                z = str2num(data_n(z_idx+1:end));
                e = 0;
                f = 0;
            end
            
        elseif ~isempty(e_idx)
            x = 0;
            y = 0;
            z = 0;
            e = str2num(data_n(e_idx+1:f_idx-1));
            if isempty(other_idx)
                f = str2num(data_n(f_idx+1:end));
            else
                f = str2num(data_n(f_idx+1:other_idx-1));
            end
            
        else
            x = 0;
            y = 0;
            z = 0;
            e = 0;
            if isempty(other_idx)
                f = str2num(data_n(f_idx+1:end));
            else
                f = str2num(data_n(f_idx+1:other_idx-1));
            end
        end
        xyzef = [x y z e f];
        if ~isempty(xyzef) && length(xyzef)==5
            try
            parsed_file.(layer_label).(type).XYZEF(xyzef_idx,:) = [x y z e f];
            xyzef_idx = xyzef_idx + 1;
            catch
                parsed_file.(layer_label).(type).XYZEF(xyzef_idx,:) = [0 0 0 0 0];
                xyzef_idx = xyzef_idx + 1;
            end
        end
    elseif contains(data_n, 'M10')
        if contains(data_n, 'M106')
            fan_on_counter = fan_on_counter + 1;
            s_idx = find(data_n == 'S');
            parsed_file.fan_on.layer_speed(fan_on_counter, :) = [layer_counter, str2num(data_n(s_idx+1:end))];
        elseif contains(data_n, 'M107')
            fan_off_counter = fan_off_counter + 1;
            parsed_file.fan_off.layer_speed(fan_off_counter) = layer_counter;
        end
%     else
%         parsed_file.MISC(misc_idx) = [layer_label,', ',type,', ',data_n,', ',num2str(n_idx)];
%         misc_idx = misc_idx + 1;
        
    end
    
end

end