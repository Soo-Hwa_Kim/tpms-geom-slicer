function virtualPrint(parsed_file)

fields = fieldnames(parsed_file.PRINTDATA);

fig1=figure;
clf
hold on;
xlabel('X')
ylabel('Y')
xlim(parsed_file.xylims(1,:))
ylim(parsed_file.xylims(2,:))
for n = 1:length(fields)
    for m = 1:length(parsed_file.PRINTDATA.(fields{n}))
        title(fields{n})
        try
            if contains(parsed_file.PRINTDATA.(fields{n}){m,2}, 'LINE')
                this_line = parsed_file.PRINTDATA.(fields{n}){m,1};
                plot(this_line(:,1),this_line(:,2),'g')
                pause(0.1)
            elseif contains(parsed_file.PRINTDATA.(fields{n}){m,2}, 'MOVE')
                this_move = parsed_file.PRINTDATA.(fields{n}){m,1};
                plot(this_move(:,1),this_move(:,2),'r')
                pause(0.1)
            end
            if n==2 && m==174
                set(fig1,'Units', 'Inches');
                pos = get(fig1, 'Position');
                set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
                print(fig1, [pwd, 'sim_print.pdf'], '-dpdf', '-r0') %save as pdf
                keyboard
            end
        catch
            keyboard
        end
    end
    
end

end