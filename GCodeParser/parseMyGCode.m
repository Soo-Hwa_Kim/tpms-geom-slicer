function parsed_file = parseMyGCode(file_name)

FID = fopen(file_name);
data = textscan(FID,'%s','Delimiter','');
fclose(FID);
data = data{1};


idx_header = 1:min(find(contains(data, 'M900 K30')));

idx_footer = max(find(contains(data, ';TYPE:Custom'))):max(find(contains(data, 'M84')));

parsed_file = struct();
parsed_file.HEADER = data(idx_header);
parsed_file.FOOTER = data(idx_footer);
print_data = data(idx_header(end)+1:idx_footer(1)-1);
brim_counter = 1;
layer_counter = -1;
part_num = 1;
already_started_brim = false;
after_brim_flag = false;
current_line = false;
layer1_flag = true;
line_matrix = [];
line1_matrix = [];
move_matrix = [];
prev_xy = [];
xmin = 100000000;
ymin = 100000000;
xmax = -100000000;
ymax = -100000000;
for n_idx = 1:length(print_data)
    data_n = print_data{n_idx};
    if contains(data_n, ';LAYER ') || contains(data_n, ';START BRIM')% layer # we are on
        if current_line && contains(data_n, ';LAYER ')
            parsed_file.PRINTDATA.(layer_label){idx_counter,1} = line_matrix;
            parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'LINE';
            current_line = false;
            idx_counter = idx_counter + 1;
            line_matrix = [];
            if contains(data_n, ';LAYER2')
                layer1_flag = false;
            end
        elseif current_line && contains(data_n, ';START BRIM')
            
        end

        if ~already_started_brim && contains(data_n,';START BRIM')
            layer_counter = layer_counter + 1;
        elseif contains(data_n, ';LAYER ')
            layer_counter = layer_counter + 1;
        end

        if contains(data_n,';START BRIM')
            brim_flag = 1;
            layer_label = ['BRIM', num2str(brim_counter)];
            brim_counter = brim_counter + 1;
            already_started_brim = true;
        else
        layer_label = ['LAYER', num2str(layer_counter)];
        end
        idx_counter = 1;
        parsed_file.PRINTDATA.(layer_label) = [];
        
    elseif contains(data_n, 'G1 X') % move x,y,z,e,f instructions
        
        x_idx = find(data_n == 'X');
        y_idx = find(data_n == 'Y');
        e_idx = find(data_n == 'E');
        f_idx = find(data_n == 'F');
        
        x = str2num(data_n(x_idx+1:y_idx-1));
        
        if contains(data_n, 'E')
            current_line = true;
            y = str2num(data_n(y_idx+1:e_idx-1));
            this_xy = [x, y];
            line_matrix = [line_matrix; this_xy];
            prev_xy = this_xy;
            xmax = max(x, xmax);
            ymax = max(y, ymax);
            xmin = min(x, xmin);
            ymin = min(y, ymin);
        else
            if current_line
                parsed_file.PRINTDATA.(layer_label){idx_counter,1} = line_matrix;
                parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'LINE';
                current_line = false;
                idx_counter = idx_counter + 1;
                line_matrix = [];
            end
            if ~isempty(f_idx)
                y = str2num(data_n(y_idx+1:f_idx-1));
            else
                y = str2num(data_n(y_idx+1:end));
            end
            this_xy = [x, y];
            if isempty(prev_xy) && layer_counter == 1
                prev_xy = this_xy;
            else
                move_matrix = [prev_xy;this_xy];
                parsed_file.PRINTDATA.(layer_label){idx_counter,1} = move_matrix;
                parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'MOVE';
                idx_counter = idx_counter + 1;
                move_matrix = [];
            end
            
        end
        
    elseif contains(data_n, ';END BRIM')
        after_brim_flag = true;
        if current_line
            parsed_file.PRINTDATA.(layer_label){idx_counter,1} = line_matrix;
            parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'LINE';
            current_line = false;
            idx_counter = 1;
            line_matrix = [];
        end
        layer_label = ['LAYER1PART', num2str(part_num)];
        part_num = part_num + 1;
        
    else
        if current_line
            parsed_file.PRINTDATA.(layer_label){idx_counter,1} = line_matrix;
            parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'LINE';
            current_line = false;
            idx_counter = idx_counter + 1;
            line_matrix = [];
        end
        parsed_file.PRINTDATA.(layer_label){idx_counter,1} = data_n;
        parsed_file.PRINTDATA.(layer_label){idx_counter,2} = 'TEXT';
        idx_counter = idx_counter + 1;
        
    end
    
end

parsed_file.xylims = [floor(xmin), ceil(xmax);floor(ymin), ceil(ymax)];

end