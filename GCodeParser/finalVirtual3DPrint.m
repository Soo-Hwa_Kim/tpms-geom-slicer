function finalVirtual3DPrint(parsed_file)

print_data = parsed_file.PRINTDATA;
layers = fieldnames(print_data);

[linesX,linesY,linesZ] = getPrintedLines(print_data, layers);

make3DPlot(linesX,linesY,linesZ, parsed_file,layers)

end

function [linesX,linesY,linesZ] = getPrintedLines(print_data, layers)

counter = 1;
for n = 1:length(layers)
   layer_data = print_data.(layers{n});
   line_idx = find(contains(layer_data(:,2),'LINE'));
   this_z = 0.2*n;
   for m = 1:length(line_idx)
       this_xy = layer_data{line_idx(m),1};
       this_z_sz = this_z*ones(size(this_xy,1));
       linesX{counter} = this_xy(:,1);
       linesY{counter} = this_xy(:,2);
       linesZ{counter} = this_z_sz;
       counter = counter + 1;
   end
end

end

function make3DPlot(linesX,linesY,linesZ, parsed_file, layers)

figure
hold on
for n = 1:length(linesX)
plot3(linesX{n}, linesY{n}, linesZ{n},'r','LineWidth',2)
end
view(3)
xlim([parsed_file.xylims(1,1) parsed_file.xylims(1,2)])
ylim([parsed_file.xylims(2,1) parsed_file.xylims(2,2)])
zlim([0 0.2*length(layers)])

end