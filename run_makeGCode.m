% run_makeGCode.m is a top-level m-file used to create GCode files for
% customised TPMS structures. This uses a direct slicing algorithm that
% generates toolpaths from an input specified TPMS, structure type, and
% geometrical specifications. This is based on and requires the geometrical
% polynomial relationships between the isovalue and geometric properties of
% TPMS. Also the implicit function and gradients function handles are 
% required for TPMS.
% Please note - this is for FDM Prusa mk3 i3 with a nozzle of 0.4mm!

%% Make customised TPMS gcode

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User fill this out:
% Please note -  I'm not sure if anyone is actually going to use this other
% than me... so I didn't put any checks in to ensure your inputs are 
% correct. So input specifications with care.

% General structure info:
tpms_alias = 'Gyroid'; %available: 'Gyroid', 'Primitive', 'Neovius', 'iWP', 'Diamond'
height_mm = 38; %outer structure dimensions in mm. height is z, length is x and y. print direction is z.
start_pt = 90; %starting point (mm) on print bed [0, 200]. This is near the centre, if you change don't go too close to the edge!!
num_periods = 4; %number of lattice cells in one dimension (ex: if enter 2, then it will be a 2x2x2 lattice)
struct_type = 2; %0 just isoline, 1 single surface structure (solid), 2 double surface structure (sheet)


% Geometric specifications:
% Enter one type of specification only (isovalue(s), minimum thickness,
% volume fraction, OR surface area)
% If you chose struct_type as 0 (isoline) specify an isovalue, don't specify a geom
% prop.
input_isovalues = []; %If don't want to specify isovalues put []. Can put in multiple!
% OR
geom_prop = [0 0.38 0]; %enter one leave rest as zero. [minthickness (mm), volume fraction (between 0 and 1), surfacearea (mm^2)]


% Gcode filename appendage at the end of the filename (OPTIONAL):
additional_insert = ''; %ex: 'Shape1'. Put '' if don't want anything.

% Done with user input section. Run block to create output gcode file :-)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Outer structure dimensions
length_mm = height_mm; 

% Output filename appendage
switch struct_type
    case 0
        insert= 'ISOLINE';
    case 1
        insert = 'SINGLE';
    case 2
        insert = 'DOUBLE';
end

% Calculate isovalues based on desired geometric properties
[isovalues, solid_filler_lines] = polyfitGeomProp(tpms_alias, struct_type, geom_prop, length_mm, input_isovalues, num_periods, start_pt);

% Calculate toolpath and generate gcode file
fhandle = @make_tpms_infill; %this can be changed to a different non-tpms shape
start_coords = [start_pt, start_pt];
finputs = {num_periods, length_mm, tpms_alias, isovalues, start_pt};
fname = [tpms_alias, '_', date, '_', insert, '_', additional_insert, '.gcode'];

makeGCode(fhandle, finputs, height_mm, start_coords, fname, solid_filler_lines);

% Simulate print (simulatePrint.m can be run by itself anytime after the
% gcode file is generated)
keyboard %I put this here since the gcode file doesn't seem to fully close by the time I run simulatePrint
simulatePrint(fname)


% END!!!

keyboard %I put this here just in case program continues to next block





%% Below are blocks used for program development and later experimental testing in PhD

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Make box gcode
height_mm = 2;
length_mm = 20;
fname = ['box_',date,'.gcode'];
start_pt = 100;
start_coords = [start_pt, start_pt];
fhandle = @makeBox;
finputs = {length_mm, start_pt};

makeGCode(fhandle, finputs, height_mm, start_coords, fname);

%% Make gyroid gcode (based on Prusa Slic3r gyroid infill)
height_mm = 80;
num_periods = 2;
fhandle = @make_gyroid_infill;
length_mm = height_mm;
start_pt = 100;
start_coords = [start_pt, start_pt];
finputs = {num_periods, length_mm, start_pt};
insert = sprintf('_%ipd_',num_periods);
additional_text = '_INFILL_TEST';
fname = ['gyroid_',date,insert,additional_text,'.gcode'];
solid_filler_lines = {};

makeGCode(fhandle, finputs, height_mm, start_coords, fname, solid_filler_lines);

simulatePrint(fname)


%% Make TPMS gcode
height_mm = 50;
num_periods = 2;
isovalues = [0 0.6]; %later this will change to a function to determine isovalues for a certain thickness
tpms = 'Neovius';
fhandle = @make_tpms_infill;
length_mm = height_mm;
start_pt = 100;
start_coords = [start_pt, start_pt];
finputs = {num_periods, length_mm, tpms, isovalues, start_pt};
insert = [];
% insert = sprintf('_%ipd_',num_periods);
additional_text = '_TPMS';
fname = [tpms, '_', date, insert,additional_text, '.gcode'];
solid_filler_lines = {};

makeGCode(fhandle, finputs, height_mm, start_coords, fname, solid_filler_lines);

simulatePrint(fname)


%% Make customised TPMS gcode

% Enter info:
tpms_alias = 'Gyroid';
height_mm = 38; length_mm = height_mm;
start_pt = 90;
input_isovalues = [-0.4 0 0.4]; %optional. If want to calculate isovalues based on geom prop enter empty array, []
num_periods = 1; %optional
struct_type = 0; %0 just isoline, 1 single structure filled, 2 double structure filled
geom_prop = [0 0 0]; %desired geom prop in mm scale. [minthickness, volume, surfacearea]

% Gcode filename insert:
switch struct_type
    case 0
        insert= 'ISOLINE';
    case 1
        insert = 'SINGLE';
    case 2
        insert = 'DOUBLE';
end
additional_insert = 'curvature';
% Calculate isovalues based on desired geometric properties
[isovalues, solid_filler_lines] = polyfitGeomProp(tpms_alias, struct_type, geom_prop, length_mm, input_isovalues, num_periods, start_pt);

fhandle = @make_tpms_infill;
start_coords = [start_pt, start_pt];
finputs = {num_periods, length_mm, tpms_alias, isovalues, start_pt};
fname = [tpms_alias, '_', date, '_TPMSCustom_',insert, additional_insert, '.gcode'];

makeGCode(fhandle, finputs, height_mm, start_coords, fname, solid_filler_lines);

simulatePrint(fname)

%% Make customised TPMS gcode -- print 5 (same shape/size) at once!
% Warning there is no check whether shapes overlap so do not change the
% size!!! ALSO filler lines is not corrected for this. So only run double
% surface structures or isolines for now.
clear all
% Enter info:
tpms_alias = 'Gyroid';
struct_type = 2; %0 just isoline, 1 single structure filled, 2 double structure filled
geom_prop = [0 0 0]; %desired geom prop in mm scale. [minthickness, volume, surfacearea]
isoval = 1.21;
input_isovalues = [-isoval isoval]; %optional. If want to calculate isovalues based on geom prop
additional_insert = 'G8f';


% Do not change!!!
height_mm = 38; length_mm = height_mm;
if height_mm ~= 38
    error('No check implemented for different sizes besides 38x38x38mm!!!')
end
start_pt = [30 30;...
            30 150;...
            90 90;...
            150 30;...
            150 150];
num_periods = 4;

% Gcode filename insert:
switch struct_type
    case 0
        insert = 'ISOLINE';
    case 1
        insert = 'SINGLE';
    case 2
        insert = 'DOUBLE';
end

% Calculate isovalues based on desired geometric properties
[isovalues, solid_filler_lines] = polyfitGeomProp(tpms_alias, struct_type, geom_prop, length_mm, input_isovalues, num_periods, start_pt);

% Make GCode
fhandle = @make_tpms_infill;
start_coords = [start_pt(1,1), start_pt(1,2)];
finputs = {num_periods, length_mm, tpms_alias, isovalues, start_pt};
fname = [tpms_alias, '_', date, '_TPMS5_', insert, additional_insert, '.gcode'];
makeGCode(fhandle, finputs, height_mm, start_pt, fname, solid_filler_lines);

% Simulate Print
disp('Pausing 30 seconds before simulation print to let gcode file close')
pause(30)
simulatePrint(fname)


%% Make customised TPMS gcode -- print multiple at once!
% 
% % Enter info:
% tpms_alias = {'Primitive'};
% height_mm = {30}; length_mm = height_mm;
% start_pt = {100};
% input_isovalues = {[-0.5 0.5]}; %optional. If want to calculate isovalues based on geom prop enter empty array, []
% num_periods = {2}; %optional
% struct_type = {2}; %0 just isoline, 1 single structure filled, 2 double structure filled
% geom_prop = {[0 0 0]}; %desired geom prop in mm scale. [minthickness, volume, surfacearea]
% 
% % Gcode filename insert:
% switch struct_type
%     case 0
%         insert= 'ISOLINE';
%     case 1
%         insert = 'SINGLE';
%     case 2
%         insert = 'DOUBLE';
% end
% additional_insert = '1';
% 
% % Double check input parts won't overlap or go beyond print bed
% checkInputDims(start_pt, length_mm);
% 
% % Calculate isovalues based on desired geometric properties
% [isovalues, solid_filler_lines] = polyfitGeomProp(tpms_alias, struct_type, geom_prop, length_mm, input_isovalues, num_periods, start_pt);
% 
% fhandle = @make_tpms_infill;
% start_coords = {[start_pt, start_pt]};
% finputs = {num_periods, length_mm, tpms_alias, isovalues, start_pt};
% fname = [tpms_alias, '_', date, '_TPMSCustom_',insert, additional_insert, '.gcode'];
% 
% makeGCode(fhandle, finputs, height_mm, start_coords, fname, solid_filler_lines);
% 
% simulatePrint(fname)
