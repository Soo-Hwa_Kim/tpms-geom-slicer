function x3 = generalFormulaGDI(x1, x2, alias, offset, phase_offset)


if nargin < 5
   
    phase_offset = 0;
    
    if nargin < 4
    
    offset = 0;
    
    end
    
end


% Get general formula coefficients depending on TPMS structure

coeff = getCoefficients(x1, x2, alias, offset, phase_offset);

x3_plus = acos(0.5 * (-coeff.one + sqrt(coeff.one .^ 2 - 4 * coeff.two))) ./ (2*pi);

x3_minus = acos(0.5 * (-coeff.one - sqrt(coeff.one .^ 2 - 4 * coeff.two))) ./ (2*pi);


% Negative root

x3_negative_plus = -x3_plus;

x3_negative_minus = -x3_minus;


% Return all non complex and non nan roots

x3 = [x3_plus x3_minus x3_negative_plus x3_negative_minus];

x3(abs(imag(x3)) > 0) = nan;

x3 = x3(~isnan(x3));


end