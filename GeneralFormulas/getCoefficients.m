function COEFF = getCoefficients(x1, x2, alias, offset, phase_offset)


if nargin < 5
    
    phase_offset = 0;
    
end

% A bit of shorthand

c1 = cos(2*pi*x1 + phase_offset);

% c1 = cos(2*pi*x1);

c2 = cos(2*pi*x2 + phase_offset);

% c2 = cos(2*pi*x2);

s1 = sin(2*pi*x1 + phase_offset);

s2 = sin(2*pi*x2 + phase_offset);

t = offset;


% Defining coefficients per TPMS

if contains(alias, 'rimitive')
    
    % Coefficients
    
    COEFF = c1 + c2 - t;
    
elseif contains(alias, 'yroid')
    
    % Intermediate terms
%     
%     if abs(s2) <= abs(c2)
        
        a = s2 ./ c1;
        
        b = (s1 .* c2 - t) ./ c1;
        
%     else
%         
%         a = c2 ./ s1;
%         
%         b = (c1 .* s2 - t) ./ s1;
%         
%     end
    
    
    % Coefficients
    
    COEFF.one = (2 * a .* b) ./ (a .^ 2 + 1);
    
    COEFF.two = (b .^ 2 - 1) ./ (a .^ 2 + 1);
    
    
elseif contains(alias, 'iamond')
    
    % Intermediate terms
    
    a = (s1 .* c2 + c1 .* s2) ./ (s1 .* s2 + c1 .* c2);
    
    b = -t ./ (s1 .* s2 + c1 .* c2);
    
    
    
    % Coefficients
    
    COEFF.one = (2 * a .* b) ./ (a .^ 2 + 1);
    
    COEFF.two = (b .^ 2 - 1) ./ (a .^ 2 + 1);
    
    
    
elseif contains(alias, 'eovius')
    
    % Coefficients
    
    COEFF = (3 * (c1 + c2) - t) ./ (3 + 4 * c1 * c2);
    
    
else
    
    % Coefficients
    
    COEFF.one = -(c1 + c2);
    
%     COEFF.two = -c1 .* c2 + 0.5 * (cos(4*pi*x1) + cos(4*pi*x2) - 1) - t;
    COEFF.two = -0.5 * (2 * c1 .* c2 - cos(4*pi*x1 + phase_offset) - cos(4*pi*x2 + phase_offset) + 1 - t);
    
    
end


end