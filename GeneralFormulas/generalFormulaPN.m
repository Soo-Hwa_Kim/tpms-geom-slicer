function x3 = generalFormulaPN(x1, x2, alias, offset)

if nargin < 3
   
    offset = 0;

end


% Get general formula coefficients depending on TPMS structure

coeff = getCoefficients(x1, x2, alias, offset);


% General formula. Pull out both positive and negative roots from arccos

x3_positive = acos(-coeff) ./ (2*pi);

x3_negative = -x3_positive;


% Return all non complex and non nan roots

x3 = [x3_positive x3_negative];

x3(abs(imag(x3)) > 0) = nan;

x3 = x3(~isnan(x3));


end