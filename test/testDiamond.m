%tpms "slices"

range = -0.5:0.01:0.5;
range = range';
[x,y,z] = meshgrid(range, range, range);
fd = neoviusFun(x,y,z);
[faces, vertices] = isosurface(x,y,z,fd);

[~,zi] = sort(vertices(:,3));
sorted_vertices = vertices(zi, :);
sorted_vertices(:,3) = round(sorted_vertices(:,3),2);
uz = unique(sorted_vertices(:,3));

figure;
hold on;
for U = 1:length(uz)
    clf
    uiter = uz(U);
    ui = find(sorted_vertices(:,3) == uiter);
    plot(sorted_vertices(ui,1), sorted_vertices(ui,2), '.');
    xlim([-0.5 0.5]);
    ylim([-0.5 0.5]);
    pause(.1);
end

