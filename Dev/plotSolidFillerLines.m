function plotSolidFillerLines(solid_filler_lines,lim)

figure;
xlim(lim)
ylim(lim)
hold on
for n = 1:length(solid_filler_lines)
    this_line = solid_filler_lines{n};
    this_x = this_line(:,:,1);
    this_y = this_line(:,:,2);
    plot(this_x(:),this_y(:))
end    

end