function test_fy(plot_title, x, z)

if nargin == 0
    
    plot_title = [];
    x = (2*pi) * [0:0.01:1]';
    z = x';
    
end

xyz = [];
verticalbool = [];
flipbool = [];
logicbool = [];

for n = 1:length(z)
    
    zlayer = z(n);
    
    [Vec2d, isvertical, isflip, islogic] = call_fy(zlayer, x);
    
    zs = zlayer * ones(length(Vec2d),1);
    
    Vec3d = [Vec2d zs];
    
    xyz = [xyz; Vec3d];
    
    verticalbool = [verticalbool; isvertical];
    
    flipbool = [flipbool; isflip];
    
    logicbool = [logicbool; islogic];
    
end

plot_fy(real(xyz), plot_title, verticalbool, flipbool, logicbool);

end