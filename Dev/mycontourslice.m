function hout = mycontourslice(x, y, z, v, sz, isovalues)

method = 'nearest';


h = [];

% Suppress the warning about the deprecated contours error output.
% oldWarn(1) = warning('off', 'MATLAB:contours:DeprecatedErrorOutputArgument');
% oldWarn(2) = warning('off', 'MATLAB:contours:EmptyErrorOutputArgument');

[xi, yi, zi] = meshgrid(x(1, :, 1), y(:, 1, 1), sz);
vi = interp3(x, y, z, v, xi, yi, zi, method);


c = contours(xi(:, :), yi(:, :), vi(:, :), isovalues);

limit = size(c, 2);
j = 1;
figure
hold on
while(j < limit)
    c_level = c(1, j);
    npoints = c(2, j);
    nextj = j + npoints + 1;
    
    xdata = c(1, j + 1 : j + npoints);
    ydata = c(2, j + 1 : j + npoints);
    plot(xdata,ydata,'.')
    
    j = nextj;
end


% warning(oldWarn);
% 
% if nargout > 0
%     hout = h;
% end
end

