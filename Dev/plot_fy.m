function plot_fy(xyz, plottitle, verticalbool, flipbool, logicbool)

x = xyz(:,1);
y = xyz(:,2);
z = xyz(:,3);

% min_val = min([min(x) min(y) min(z)]);
% max_val = max([max(x) max(y) max(z)]);

range = linspace(0, (2*pi), 500);
[gx, gy, gz] = meshgrid(range, range, range);
gf = gyroidFun(gx/(2*pi), gy/(2*pi), gz/(2*pi));
[faces, vertices] = isosurface(gx, gy, gz, gf);

% figure;
% isosurface(gx, gy, gz, gf); hold on
% scatter3(x, y, z, 'filled')
% xlabel('x')
% ylabel('y')
% zlabel('z')
% if ~isempty(title)
%     title(title)
% end

% figure
% scatter3(x, y, z, 'filled')
% xlabel('x')
% ylabel('y')
% zlabel('z')
% if ~isempty(title)
%     title(title)
% end

verticalbool = logical(verticalbool);

figure
scatter3(x(verticalbool), y(verticalbool), z(verticalbool), 'filled');hold on
scatter3(x(~verticalbool), y(~verticalbool), z(~verticalbool), 'filled')
xlabel('x')
ylabel('y')
zlabel('z')
legend('isvertical', '~isvertical')


logicbool = logical(logicbool);

figure
scatter3(x(logicbool), y(logicbool), z(logicbool), 'filled');hold on
scatter3(x(~logicbool), y(~logicbool), z(~logicbool), 'filled')
xlabel('x')
ylabel('y')
zlabel('z')
legend('islogic', '~islogic')

flipbool = logical(flipbool);

xf = x(flipbool); yf = y(flipbool); zf = z(flipbool);
xnf = x(~flipbool); ynf = y(~flipbool); znf = z(~flipbool);

figure
scatter3(xf, yf, zf, 'filled');hold on
scatter3(xnf, ynf, znf, 'filled')
xlabel('x')
ylabel('y')
zlabel('z')
legend('isflip', '~isflip')

%Layer plotting

%First layer at z = 0
z0_ind = find(round(vertices(:,3), 2) == 0);
z0_ind2 = find(round(zf,8)==0);
z0_ind3 = find(round(znf,8)==0);

figure
scatter3(vertices(z0_ind, 1), vertices(z0_ind, 2), vertices(z0_ind, 3), 'filled');hold on
scatter3(xf(z0_ind2), yf(z0_ind2), zf(z0_ind2), 'filled');
scatter3(xnf(z0_ind3), ynf(z0_ind3), znf(z0_ind3), 'filled');
xlabel('x')
ylabel('y')
zlabel('z')
zlim([-0.01 2*pi])
legend('isosurface', 'infill flip', 'infill ~flip')
title('zlayer = 0')

%Middle layer at z = pi
z0_ind = find(round((vertices(:,3)-pi), 2) == 0);
% z0_ind2 = find(z==pi);
% 
% figure
% scatter3(vertices(z0_ind, 1), vertices(z0_ind, 2), vertices(z0_ind, 3), 'filled');hold on
% scatter3(x(z0_ind2), y(z0_ind2), z(z0_ind2), 'filled');
% xlabel('x')
% ylabel('y')
% zlabel('z')
% zlim([-0.01 2*pi])
% legend('isosurface', 'infill')

z0_ind2 = find(zf==pi);
z0_ind3 = find(znf==pi);

figure
scatter3(vertices(z0_ind, 1), vertices(z0_ind, 2), vertices(z0_ind, 3), 'filled');hold on
scatter3(xf(z0_ind2), yf(z0_ind2), zf(z0_ind2), 'filled');
scatter3(xnf(z0_ind3), ynf(z0_ind3), znf(z0_ind3), 'filled');
xlabel('x')
ylabel('y')
zlabel('z')
zlim([-0.01 2*pi])
legend('isosurface', 'infill flip', 'infill ~flip')
title('zlayer = pi')

%Layer at z = pi/2
z0_ind = find(round((vertices(:,3)-pi/2), 2) == 0);

z0_ind2 = find(zf==pi/2);
z0_ind3 = find(znf==pi/2);

figure
scatter3(vertices(z0_ind, 1), vertices(z0_ind, 2), vertices(z0_ind, 3), 'filled');hold on
scatter3(xf(z0_ind2), yf(z0_ind2), zf(z0_ind2), 'filled');
scatter3(xnf(z0_ind3), ynf(z0_ind3), znf(z0_ind3), 'filled');
xlabel('x')
ylabel('y')
zlabel('z')
zlim([-0.01 2*pi])
legend('isosurface', 'infill flip', 'infill ~flip')
title('zlayer = pi/2')

%Layer at z = 3pi/2
z0_ind = find(round((vertices(:,3)-3*pi/2), 2) == 0);

z0_ind2 = find(zf==3*pi/2);
z0_ind3 = find(znf==3*pi/2);

figure
scatter3(vertices(z0_ind, 1), vertices(z0_ind, 2), vertices(z0_ind, 3), 'filled');hold on
scatter3(xf(z0_ind2), yf(z0_ind2), zf(z0_ind2), 'filled');
scatter3(xnf(z0_ind3), ynf(z0_ind3), znf(z0_ind3), 'filled');
xlabel('x')
ylabel('y')
zlabel('z')
zlim([-0.01 2*pi])
legend('isosurface', 'infill flip', 'infill ~flip')
title('zlayer = 3pi/2')

end