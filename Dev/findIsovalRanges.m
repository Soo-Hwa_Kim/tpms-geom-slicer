close all
alias = 'iWP';
isovalue = 3;

fhandle = lookupTPMSHandleAndOffsets(alias);
range = linspace(-0.5,0.5,300);
[X,Y,Z] = meshgrid(range,range,range);
fvals = fhandle(X,Y,Z);

figure
isosurface(X,Y,Z,fvals,isovalue)
title([alias, ', isoval = ',num2str(isovalue)])

