function isosurface_inputs = isosurfaceGyroid(range)

if nargin < 1
    range = 0:0.01:1;
end

[x, y, z] = meshgrid(range, range, range);
f = gyroidFun(x, y, z);

isosurface_inputs = {2*pi*x, 2*pi*y, 2*pi*z, f};

end