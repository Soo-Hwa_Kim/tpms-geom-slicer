function [Vec2d, isvertical, isflip, islogic] = call_fy(z, x)

zsin = round(sin(z), 8);
zcos = round(cos(z), 8);

vertical = zsin <= zcos;

yt = zeros(length(x), 1);
yf = zeros(length(x), 1);

for n = 1:length(x)
    
    flip = true;
    
    yt(n) = fy(x(n), zsin, zcos, vertical, flip);
    
    if any(imag(yt))
        yt(n) = nan;
    end
    
    if vertical && flip
        
        if round(zcos,8) < 0
            yt(n) = yt(n) + pi;
        end
    end
    
    
    
    flip = false;
    
    yf(n) = fy(x(n), zsin, zcos, vertical, flip);
    
    if any(imag(yf))
        yf(n) = nan;
    end
    
    if ~vertical && ~flip
        yf(n) = yf(n) + pi;
    end
    
    if vertical && ~flip
        if round(cos(x(n)-pi/2),8) < 0
            yf(n) = yf(n) - pi;
        else
            yf(n) = yf(n) + pi;
        end
    end
    
end

if ~vertical
    Vec2d = [yt, x; yf, x];
    isvertical = false(size(Vec2d,1), 1);
    if round(zcos,8) < 0
        islogic = true(size(Vec2d,1), 1);
    else
        islogic = false(size(Vec2d,1), 1);
    end
else
    Vec2d = [x, yt; x, yf];
    isvertical = true(size(Vec2d,1), 1);
    if round(zsin,8) < 0
        islogic = true(size(Vec2d,1), 1);
    else
        islogic = false(size(Vec2d,1), 1);
    end
end

isflip = [true(size(yt,1), 1); false(size(yf,1), 1)];

% Vec2d = [x, yt; x, yf];


end