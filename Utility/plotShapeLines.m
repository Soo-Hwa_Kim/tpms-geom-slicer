function plotShapeLines(lines, layer, scale_factor)

[curv_minmax_sf,xminmax,yminmax, Km] = curvScaleFactor(lines, scale_factor);
figure;
histogram(Km)
xlabel('mean curvature distribution')
plot_curvatures = false;
if contains(class(lines),'cell')
    figure
    clf
    xlim([xminmax(1) xminmax(2)])
    ylim([yminmax(1) yminmax(2)])
    title(sprintf('GCODE Layer %i',layer))
    hold on
    if plot_curvatures
        for n = 1:length(lines)
            this_line = lines{n};
            col = curvatureColors(this_line, curv_minmax_sf, scale_factor);
            x = this_line(:,:,1);
            y = this_line(:,:,2);
            z = zeros(size(this_line(:,:,1)));
            if size(this_line, 3) > 1
                %plot(this_line(:,:,1),this_line(:,:,2))
                surface([x;x],[y;y],[z;z],[col;col],...
                    'facecol','no',...
                    'edgecol','interp',...
                    'linew',2)
            else
                plot(this_line(:,1),this_line(:,2))
            end
            pause(0.2)
        end
    else
        for n = 1:length(lines)
            this_line = lines{n};
            if size(this_line, 3) > 1
                plot(this_line(:,:,1),this_line(:,:,2),'r','LineWidth',2)
            else
                plot(this_line(:,1),this_line(:,2))
            end
            pause(0.2)
        end
    end
    
else
    xmax = ceil(max(max(lines(:,:,1))));
    xmin = floor(min(min(lines(:,:,1))));
    ymax = ceil(max(max(lines(:,:,2))));
    ymin = floor(min(min(lines(:,:,2))));
    
    figure
    clf
    xlim([xmin xmax])
    ylim([ymin ymax])
    title(sprintf('Maths Layer %i',layer))
    hold on
    for n = 1:size(lines,1)
        plot(lines(n,:,1),lines(n,:,2))
        pause(0.2)
    end
end
end