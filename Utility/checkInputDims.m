function checkInputDims(start_pt, length_mm)

checkWithinBedLims(start_pt, length_mm);

checkForIntersection(start_pt, length_mm);

end

function checkWithinBedLims(start_pt, length_mm)

print_bed_dims = 20:200;

for n = 1:length(length_mm)
    this_start_pt = start_pt{n};
    this_part_range = [this_start_pt(1):length_mm; this_start_pt(2):length_mm];
    test = ismember(this_part_range, print_bed_dims);
    if any(~test)
        error_msg = sprintf('Part #%i is not within print bed dimensions\n', n);
        error(error_msg);
    end
end

end


function checkForIntersection(start_pt, length_mm)
tol_mm = 3; %Adding a bit of tolerance to account for filament thickness
start_pt_tol = cellfun(@(x) x-tol_mm, start_pt, 'un', 0);
length_mm_tol = cellfun(@(x) x+tol_mm, length_mm, 'un', 0);

for n = 1:length(length_mm_tol)
    this_start_pt = start_pt_tol{n};
    polyshapes{n} = polyshape([this_start_pt(1) this_start_pt(1) this_start_pt(1)+length_mm_tol{n} this_start_pt(1)+length_mm_tol{n}],...
        [this_start_pt(2) this_start_pt(2)+length_mm_tol{n} this_start_pt(2) this_start_pt(2)+length_mm_tol{n}]);
    
end
polyvec = [polyshapes{:}];
tf = overlaps(polyvec);
tf_no_diag = tf-diag(diag(tf));

if any(any(tf_no_diag))
    error('Parts overlap!')
end

end