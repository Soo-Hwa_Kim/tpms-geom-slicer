function translated_lines = translateLines(lines, translate)

for n = 1:length(lines)
    this_line = lines{n};
    xlines = this_line(:,:,1);
    ylines = this_line(:,:,2);
    xlines = xlines + translate(1);
    ylines = ylines + translate(2);
    this_translated_lines(:,:,1) = xlines;
    this_translated_lines(:,:,2) = ylines;
    translated_lines{n} = this_translated_lines; %#okAGROW
    clear xlines ylines this_translated_lines
end

end