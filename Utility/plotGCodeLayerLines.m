function plotGCodeLayerLines(layer, layernum)


layer_counter = 0;
current_line = false;
all_lines = [];
idx_counter = 1;
line_matrix = [];
xmin = 100000000;
ymin = 100000000;
xmax = -100000000;
ymax = -100000000;
for n_idx = 1:length(layer)
    data_n = layer{n_idx};
    if contains(data_n, 'G1 X') % move x,y,z,e,f instructions
        
        x_idx = find(data_n == 'X');
        y_idx = find(data_n == 'Y');
        e_idx = find(data_n == 'E');
        f_idx = find(data_n == 'F');
        
        x = str2num(data_n(x_idx+1:y_idx-1));
        
        if contains(data_n, 'E')
            current_line = true;
            y = str2num(data_n(y_idx+1:e_idx-1));
            this_xy = [x, y];
            line_matrix = [line_matrix; this_xy];
            prev_xy = this_xy;
            xmax = max(x, xmax);
            ymax = max(y, ymax);
            xmin = min(x, xmin);
            ymin = min(y, ymin);
            if n_idx == length(layer)
                all_lines{idx_counter} = line_matrix;
                idx_counter = idx_counter + 1;
            end
            
        else
            if current_line
                all_lines{idx_counter} = line_matrix;
                current_line = false;
                idx_counter = idx_counter + 1;
                line_matrix = [];
            end
        end
    else
        if current_line
            all_lines{idx_counter} = line_matrix;
            current_line = false;
            idx_counter = idx_counter + 1;
            line_matrix = [];
        end
        
    end
    
end
xylims = [xmin xmax;ymin ymax];
all_lines{idx_counter} = xylims;
plotShapeLines(all_lines, layernum);

end