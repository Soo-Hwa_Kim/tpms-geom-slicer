function [Kg, Km] = curvatureGyroid(vertices)

Km = zeros(size(vertices, 1), 1);
Kg = zeros(size(vertices, 1), 1);

for n = 1:size(vertices, 1)
    
    x = (2*pi)*vertices(n,1);
    y = (2*pi)*vertices(n,2);
    z = (2*pi)*vertices(n,3);
    
    Fx = -sin(x).*sin(y) + cos(z).*cos(x);   
    Fy = cos(x).*cos(y) - sin(y).*sin(z); 
    Fz = cos(y).*cos(z) - sin(z).*sin(x);

    Fxx = -cos(x).*sin(y) - cos(z).*sin(x);    
    Fxy = -sin(x).*cos(y);    
    Fxz = -sin(z).*cos(x);
    Fyx = -sin(x).*cos(y);
    Fyy = -cos(x).*sin(y) - cos(y).*sin(z);    
    Fyz = -sin(y).*cos(z);    
    Fzx = -sin(z).*cos(x);   
    Fzy = -sin(y).*cos(z);    
    Fzz = -cos(y).*sin(z) - cos(z).*sin(x);
      
    Delta_F = [Fx, Fy, Fz];
 
    H_F = [Fxx, Fxy, Fxz;...
           Fyx, Fyy, Fyz;...
           Fzx, Fzy, Fzz];  
    
    H_star = [Fyy*Fzz-Fyz*Fzy,  Fyx*Fzx-Fyx*Fzz,  Fyx*Fzy-Fyy*Fzx;...
              Fxz*Fzy-Fxy*Fzz,  Fxx*Fzz-Fxz*Fzx,  Fxy*Fzx-Fxx*Fzy;...
              Fxy*Fyz-Fxz*Fyy,  Fyx*Fxz-Fxx*Fyz,  Fxx*Fyy-Fxy*Fyx];
    
    
    
    Km(n) = Delta_F * H_F * Delta_F' - norm(Delta_F,2)^2 * trace(H_F) / (2 * norm(Delta_F,2)^3);
    Kg(n) = Delta_F * H_star * Delta_F' / norm(Delta_F,2)^4;
    
end

end