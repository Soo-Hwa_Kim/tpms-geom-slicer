function [curv_minmax_sf, xminmax, yminmax, Km] = curvScaleFactor(lines, scale_factor)

x = [];
y = [];
for n = 1:length(lines)
   this_line = lines{n};
   x = [x;this_line(:,:,1)'];
   y = [y;this_line(:,:,2)'];   
end

z = 0.2*ones(size(x));
vertices = [x*scale_factor, y*scale_factor, z];

[Kg, Km] = curvatureGyroid(vertices);

curv_minmax_sf = [min(Km), max(Km)];
xminmax = [floor(min(x)) ceil(max(x))];
yminmax = [floor(min(y)) ceil(max(y))];

end