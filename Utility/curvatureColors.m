function colors = curvatureColors(this_line, curv_minmax_sf, scale_factor)

x = this_line(:,:,1) *scale_factor;
y = this_line(:,:,2) *scale_factor;
z = 0.2*ones(size(x));
vertices = [x' y' z'];

[Kg, Km] = curvatureGyroid(vertices);
Km_scaled = (Km-curv_minmax_sf(1))./(curv_minmax_sf(2)-curv_minmax_sf(1));
colors = Km_scaled';
% colors = zeros(length(Km_scaled), 3);
% for n = 1:length(Km_scaled)
%    Km_n = Km_scaled(n);
%    if Km_n > 0
%        colors(n,:) = [Km_n, 1-Km_n, 0];
%    else
%        colors(n,:) = [0, 1+Km_n, Km_n];
%    end
% end

end