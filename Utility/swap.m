function [x, y] = swap(x, y)

temp = x;
x = y;
y = temp;

end