function alias = checkAlias(alias)
    if contains(alias, 'rimitive') || contains(alias, 'RIMITIVE')
        alias = 'Primitive';
    elseif contains(alias, 'yroid') || contains(alias, 'YROID')
        alias = 'Gyroid';
    elseif contains(alias, 'eovius') || contains(alias, 'EOVIUS')
        alias = 'Neovius';
    elseif contains(alias, 'iamond') || contains(alias, 'IAMOND')
        alias = 'Diamond';
    else
        alias = 'iWP';
    end
end