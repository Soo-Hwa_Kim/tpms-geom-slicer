function scale_factor = findScaleFactor(num_periods, length_mm)
% mm to unit cell

%stuff to always run
height = num_periods; %x max

% scaling factor between pure one unit cell of lattice to size and location
% on the printer
scale_factor = height/length_mm;

end