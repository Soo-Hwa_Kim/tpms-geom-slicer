function array = char2numcell(mycell)

for n = 1:length(mycell)
   array(n) = str2num(mycell{n}); 
end

end