
range = linspace(0,1,100);
[X, Y, Z] = meshgrid(range, range, range);

fun = @gyroidFun;
fg = fun(X, Y, Z);

zslice = 0.1;
isovalues =[0 0.5];
hout = mycontourslice(X,Y,Z,fg,zslice,isovalues);

keyboard


