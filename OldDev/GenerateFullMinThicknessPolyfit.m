function GenerateFullMinThicknessPolyfit(aliases)


% Delete any prior polyfit tolerance logs
polyfit_dir = fullfile([pwd,'\FullMinThicknessPolyfit\Outputs\Polyfit\ToleranceLog\']);
if exist(polyfit_dir, 'dir')
    delete FullMinThicknessPolyfit\Outputs\Polyfit\ToleranceLog\*.txt
end


%% Loop through all input surface aliases

for n = 1:length(aliases)
    
    alias = aliases{n};
    
    
    %% Set up TPMS information
    
    % TPMS information for each offset is stored in the structure
    % SURFACE_DATA to be used to calculate each of the geometric
    % properties
    
    % Store alias and root directory
    
    SURFACE_DATA = setupSurfaceDataStructureFMT(alias);
    
    
    % Prepare TPMS data within structure
    
    SURFACE_DATA = prepareTPMSDataFMT(SURFACE_DATA);
    
    
    %% Generate Raw Data for Geometric Properties
    
    if SURFACE_DATA.flags.calculate_double_surface_thickness_flag
         
    generateThicknessDataFMT(SURFACE_DATA)

    end
    
    %% Create visuals of the data
    
    setupAndGeneratePlotsFMT(SURFACE_DATA);
    
    
end


end
