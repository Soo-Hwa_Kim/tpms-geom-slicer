function geom_prop = calculateGeomPropPolyfit(coeff_arr, isovalue1, isovalue2)
if contains(class(coeff_arr), 'cell')
    try
        coeff_arr = char2numcell(coeff_arr);
    catch
        coeff_arr = coeff_arr{1};
        coeff_arr = char2numcell(coeff_arr);
    end
end
if nargin == 2
    geom_prop = calculateIsovalue2DPolyfit(coeff_arr, isovalue1);
else
    geom_prop = calculateIsovalue3DPolyfit(coeff_arr, isovalue1, isovalue2);
end

end

function geom_prop = calculateIsovalue2DPolyfit(bn, isovalue1)

power_offset = powerxMatrix(isovalue1, length(bn)-1);
geom_prop = sum(bn .* power_offset);

end


function geom_prop = calculateIsovalue3DPolyfit(bn, X, Y)
bn = bn';
bn_counter = 0;
geom_prop = zeros(length(X),1);
lowest_degree = max(roots([1, 1, -2*length(bn)]))-1;
for degn = 1:lowest_degree+1
   ypower = 0:degn-1;
   Ypower = Y.^(ypower);
   xpower = fliplr(ypower);
   Xpower = X.^(xpower);
   bn_idx = bn_counter+1:bn_counter+length(ypower);
   this_test = sum(Ypower .* Xpower .* repmat(bn(bn_idx)',size(Ypower,1),1), 2);
   try
   geom_prop = geom_prop+this_test;
   catch
       keyboard
   end
   
   bn_counter = bn_counter+length(ypower);
   
end

end