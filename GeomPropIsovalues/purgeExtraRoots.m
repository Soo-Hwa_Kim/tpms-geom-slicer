function main_isovalues = purgeExtraRoots(main_isovalues, alias, geom_prop_unitcell, struct_type, coeffs)

% Purge outside lattice range
[~, ~, lattice_range_isovalues, ~] = lookupTPMSHandleAndOffsets(alias);
isvalid = main_isovalues(main_isovalues >= lattice_range_isovalues(1) & main_isovalues <= lattice_range_isovalues(end));
isvalid = reshape(isvalid, [], struct_type);

% Purge roots not close to what we want
if numel(isvalid) ~= struct_type
    
    coeffs = char2numcell(coeffs);
    
    calc_geom_prop = zeros(size(isvalid,1), 1);
    for n = 1:size(isvalid,1)
        sorted_isvalid_n = sort(isvalid(n,:));
        if struct_type==2
            calc_geom_prop(n) = calculateGeomPropPolyfit(coeffs, sorted_isvalid_n(1), sorted_isvalid_n(2));
        else
            calc_geom_prop(n) = calculateGeomPropPolyfit(coeffs, sorted_isvalid_n(1));
        end
    end
    
    % See which isovalue is closest to what we want
    [~, min_idx] = min(abs(calc_geom_prop(n)-geom_prop_unitcell));
    isvalid = isvalid(min_idx, :);
end

main_isovalues = sort(isvalid);

end