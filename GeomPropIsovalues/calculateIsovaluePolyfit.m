function geom_prop = calculateIsovaluePolyfit(coeff_arr, isovalue1, isovalue2)

if nargin == 2
    geom_prop = calculateIsovalue2DPolyfit(coeff_arr, isovalue1);
else
    geom_prop = calculateIsovalue3DPolyfit(coeff_arr, isovalue1, isovalue2);
end

end

function geom_prop = calculateIsovalue2DPolyfit(bn, isovalue1)

power_offset = powerxMatrix(isovalue1, length(bn)-1);
bns = repmat(bn',length(isovalue1),1);
geom_prop = sum(bns .* power_offset,2);

end


function geom_prop = calculateIsovalue3DPolyfit(coeff_arr, isovalue1, isovalue2)

bn_counter = 0;
geom_prop = zeros(length(X),1);
for degn = 1:lowest_degree+1
   ypower = 0:degn-1;
   Ypower = Y.^(ypower);
   xpower = fliplr(ypower);
   Xpower = X.^(xpower);
   bn_idx = bn_counter+1:bn_counter+length(ypower);
   this_test = sum(Ypower .* Xpower .* repmat(bn(bn_idx)',length(Ypower),1), 2);
   geom_prop = geom_prop+this_test;
   
   bn_counter = bn_counter+length(ypower);
   
end

end