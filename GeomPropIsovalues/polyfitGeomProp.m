function [isovalues, solid_filler_lines] = polyfitGeomProp(alias, struct_type, geom_prop, length_mm, input_isovalues, num_periods, start_pt)

if struct_type == 0 %if just isoline then sort and return. nothing to calculate.
    isovalues = sort(input_isovalues);
    solid_filler_lines = {};
    testMainIsovalues(isovalues, alias);
    
else
    %import geometric polynomial relationship coefficients
    coeffs = importPolyCoeff(alias,struct_type,geom_prop);
    
    %calculate main isovalues
    if isempty(input_isovalues)%if geom_prop specified
        scale_factor = findScaleFactor(num_periods, length_mm);
        geom_prop_type = find(geom_prop ~= 0);
        switch geom_prop_type %scale to unitcell based on geom prop type (mt, vf, sa)
            case 1 %minimum thickness
                geom_prop_unitcell = geom_prop * scale_factor;
            case 2 %volume fraction
                geom_prop_unitcell = geom_prop;
            case 3 %surface area
                geom_prop_unitcell = geom_prop * scale_factor^2;
        end
        
        [main_isovalues, struct_type] = calculateMainIsovalues(coeffs{1}, struct_type, geom_prop_unitcell);
        main_isovalues = purgeExtraRoots(main_isovalues, alias, nonzeros(geom_prop_unitcell), struct_type, coeffs{1});
        [main_isovalues, struct_type] = adjustMainIsovalues(coeffs{2}, struct_type, main_isovalues, scale_factor, alias);
        
    else %if isovals are specified
        main_isovalues = input_isovalues;
        
    end
    
    %check if main isovalues are within valid lattice range for the tpms.
    %will throw error if not valid!
    testMainIsovalues(main_isovalues, alias);
    
    %if struct_type changed to isoline return isovalue
    if struct_type == 0
        isovalues = sort(input_isovalues);
        solid_filler_lines = {};
        return;
    end
    
    %calculate filler isovalues between main isovalue(s). solid filler
    %lines is solid hatching lines where lattice doesn't exist for single
    %surface structures.
    [filler_isovalues, solid_filler_lines] = calculateFillerIsovalues(alias, main_isovalues, coeffs{end}, num_periods, length_mm, struct_type, start_pt);
    
    % sort in order of printing. prints by isovalue starting with isovalue
    % closest to zero.
    try
        isovalues = sortIsovalues([main_isovalues filler_isovalues]);
    catch
        isovalues = sortIsovalues([main_isovalues' filler_isovalues]);
    end
    
end

end



function coeffs = importPolyCoeff(alias,struct_type,geom_prop)
% import needed coefficients from geometric relationships

alias = checkAlias(alias);
[geom_prop_names, singleordouble] = getNames(struct_type);

coeff_dir = [pwd, '\FittingCoefficients\', alias, '\'];
geom_prop_idx = find(geom_prop ~= 0);
coeffs = cell(length(geom_prop_idx)+1,1);

if struct_type == 1
    ns = 3;
else
    ns = 2;
end

for n = 1:ns
    this_idx = n;
    if n == 1
        fname = [alias, 'Coeff', geom_prop_names{geom_prop_idx,1}, singleordouble, '.txt'];
        
    elseif n == 2
        if struct_type == 1
            fname = [alias, 'Coeff', 'Thickness', 'Single', '.txt'];
        else
            fname = [alias, 'Coeff', 'Thickness', 'Double', '.txt'];
        end
    else
        fname = [alias, 'Coeff', 'Thickness', 'Double', '.txt'];
    end
    file_name = fullfile([coeff_dir,fname]);
    FID = fopen(file_name);
    this_coeff = textscan(FID,'%s','Delimiter','');
    fclose(FID);
    coeffs{this_idx} = this_coeff{:};
end


end

function [geom_prop_names, singleordouble] = getNames(struct_type)
%For importing coefficient filesnames

if struct_type == 1
    singleordouble = 'Single';
elseif struct_type == 2
    singleordouble = 'Double';
else
    singleordouble = [];
end

geom_prop_names = {'Thickness';'Volume';'SurfaceArea'};

end

function [filler_isovalues, solid_filler_lines] = calculateFillerIsovalues(alias, main_isovalues, coeff_cell, num_periods, length_mm, struct_type, start_pt)
coeff_arr = char2numcell(coeff_cell);

filament_thickness_mm = 0.35; % a little closer than 0.4
scale_factor = findScaleFactor(num_periods, length_mm);
filament_thickness_unitcell = filament_thickness_mm * scale_factor;
[fhandle, ~, lattice_range_isovalues, ~] = lookupTPMSHandleAndOffsets(alias);
if struct_type == 1
    end_isovalue = lattice_range_isovalues(end);
elseif struct_type == 2
    end_isovalue = max(main_isovalues);
end

this_isovalue = min(main_isovalues);
thickness_btwn_isovals = calculateGeomPropPolyfit(coeff_arr, this_isovalue, end_isovalue);
% thickness_btwn_isovals = calculateGeomPropPolyfit(coeff_arr, end_isovalue, this_isovalue);
num_isolines = floor(thickness_btwn_isovals/filament_thickness_unitcell);
thickness_btwn_fillers = thickness_btwn_isovals/num_isolines;
if thickness_btwn_fillers/scale_factor < filament_thickness_mm
    num_isolines = num_isolines - 1;
    thickness_btwn_fillers = thickness_btwn_isovals/num_isolines;
end
filler_isovalues = zeros(1, num_isolines-1);
for n = 1:num_isolines-1
    
    isovalue_geomprop = calculateIsovalForGeomProp(coeff_arr, thickness_btwn_fillers, this_isovalue);
    isovalue_geomprop = purgeOutOfRangeIsovals(isovalue_geomprop, lattice_range_isovalues);
    if size(isovalue_geomprop,1) ~= 1
        keyboard
        
    end
    filler_isovalues(n) = isovalue_geomprop(1);
    this_isovalue = isovalue_geomprop(1);
    
end

%  figure
if struct_type == 1
    Z = 0.2:0.2:length_mm;
    for m = 1:length(Z)
        vertical_flag = logical(mod(m,2));
        z_mm = Z(m);
        
        [solid_filler_lines_z, f_at_z] = createSolidFillerLinesIsoval(end_isovalue, fhandle, coeff_arr, num_periods, length_mm, z_mm, Z, vertical_flag, start_pt);
        %          imagesc(f_at_z)
        %         pause(0.1)
        
        solid_filler_lines{m} = solid_filler_lines_z;
    end
    
else
    solid_filler_lines = {};
end


end

function sorted_isovalues = sortIsovalues(isovalues)
% Sort in the order starting with isovalue closest to zero then print
% outwards

[~, sort_idx] = sort(abs(isovalues));
sorted_isovalues = isovalues(sort_idx);



end

function isovalue_geomprop = purgeOutOfRangeIsovals(isovalue_geomprop, lattice_range_isovalues)

idx1 = find(isovalue_geomprop(:,1) < max(lattice_range_isovalues));
idx2 = find(isovalue_geomprop(:,1) > min(lattice_range_isovalues));
isovalue_geomprop = isovalue_geomprop(intersect(idx1,idx2),:);

end