function interp_mesh = getInterpolatedMesh(coeff_arr, isovalue_range, struct_type)

if struct_type == 1
    for n = 1:length(isovalue_range)
        geom_prop(n) = calculateGeomPropPolyfit(coeff_arr, isovalue_range(n));
    end
    interp_mesh = [geom_prop, isovalue_range];
elseif struct_type == 2
    isovalue_combos = nchoosek(isovalue_range,2);
    for n = 1:length(isovalue_combos)
        geom_prop(n) = calculateGeomPropPolyfit(coeff_arr, isovalue_combos(n,1), isovalue_combos(n,2));
    end
    interp_mesh = [geom_prop, isovalue_combos];
end
end