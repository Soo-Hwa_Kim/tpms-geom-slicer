function [adjusted_main_isovalues, struct_type] = adjustMainIsovalues(coeffs, struct_type, main_isovalues, scale_factor, alias)
%Adjusts isovalues for actual thickness of the filament. If they are too
%close for nozzle/filament, it will switch to isoline and use an average of
%the isovalues.

try
    coeffs = char2numcell(coeffs);
catch
    coeffs = coeffs{1};
    coeffs = char2numcell(coeffs);
end
filament_thickness = 0.35; %This is set to 0.35mm instead of 0.4mm which is
%nozzle diameter. This is because the toolpaths
%are based on a 0.35mm minimum thickness.

% Calculate current minimum thickness and check they are not too close
if struct_type==2
    current_thickness = calculateGeomPropPolyfit(coeffs, main_isovalues(1), main_isovalues(2));
    if current_thickness < 0
        current_thickness = calculateGeomPropPolyfit(coeffs, main_isovalues(2), main_isovalues(1));
    end
else
    current_thickness = calculateGeomPropPolyfit(coeffs, main_isovalues(1));
end

if current_thickness/scale_factor < filament_thickness
    adjusted_main_isovalues = mean(main_isovalues);
    struct_type = 0;
    return;
end

% Adjust thickness and check they are not too close
adjusted_thickness = (current_thickness/scale_factor - filament_thickness)*scale_factor; %check this is correct for single surface structures before using

if adjusted_thickness/scale_factor < filament_thickness
    adjusted_main_isovalues = mean(main_isovalues);
    struct_type = 0;
    return;
end

% Calculate adjusted isovalues
if struct_type~=1
    fake_arg_in = 'not double';
    adjusted_main_isovalues = calculateIsovalForGeomProp(coeffs, adjusted_thickness, fake_arg_in);
else
    adjusted_main_isovalues = calculateIsovalForGeomProp(coeffs, adjusted_thickness);
end

% Only use roots within tpms lattice range
adjusted_main_isovalues = withinLatticeRange(adjusted_main_isovalues, alias, struct_type, main_isovalues);

end


function adjusted_main_isovalues = withinLatticeRange(adjusted_main_isovalues, alias, struct_type, main_isovalues)

[~, ~, lattice_range_isovalues, ~] = lookupTPMSHandleAndOffsets(alias);
adjusted_main_isovalues = adjusted_main_isovalues(adjusted_main_isovalues >= lattice_range_isovalues(1) & adjusted_main_isovalues <= lattice_range_isovalues(end));

if numel(adjusted_main_isovalues) ~= struct_type
    adjusted_main_isovalues = adjusted_main_isovalues(adjusted_main_isovalues >= main_isovalues(1) & adjusted_main_isovalues <= main_isovalues(end));
end

adjusted_main_isovalues = sort(adjusted_main_isovalues);

end
