function isovalue_geomprop = calculateIsovalForGeomProp(varargin)
% This assumes coeff are imported as is and are in the orientation of y =
% a0 + a1*x + a2*x^2 + ... + an*x^n. coeff should be 1xn array. Returns a
% nx2 matrix with sorted isovalues in first column and respective geom prop
% in second.

an = varargin{1};
geom_prop = varargin{2};
isovalue_geomprop = [];
if length(varargin) < 3 %single tpms structure
    geom_prop = geom_prop(geom_prop~=0);
    
    coeff = an;
    coeff(1) = coeff(1) - geom_prop;
    allroots = roots(fliplr(coeff));
    idx_root = find(round(imag(allroots), 5) == 0);
    isovalue_geomprop = real(allroots(idx_root));
    
elseif contains(class(varargin{3}), 'double') %double tpms structure but one isovalue is known
    known_isovalue = varargin{3};
    for n = 1:length(geom_prop)
        cn = calcNewCoeffKnownIsoval(an, geom_prop(n), known_isovalue);
        allroots = roots(fliplr(cn));
        idx_root = find(round(imag(allroots), 5) == 0);
        valid_roots = real(allroots(idx_root));
        valid_roots = valid_roots(valid_roots > known_isovalue);
        this_isovalue_geomprop = [valid_roots, geom_prop*ones(size(valid_roots))];
        isovalue_geomprop = [isovalue_geomprop;this_isovalue_geomprop]; %#okAGROW
    end
    [~,ia] = sort(isovalue_geomprop(:,1));
    isovalue_geomprop = isovalue_geomprop(ia,:);
    
    
else %double tpms structure but absolute value of isovalues [x,y] are equal abs(x) = abs(y)
    geom_prop = geom_prop(geom_prop~=0);
    try
        cn = calcNewCoeffSameAbsIsoval(an, geom_prop);
    catch
        cn = calcNewCoeffSameAbsIsovalalt(an, geom_prop);
    end
    allroots = roots(fliplr(cn));
    idx_root = find(round(imag(allroots), 5) == 0);
    valid_roots = real(allroots(idx_root));
%     valid_roots = valid_roots(valid_roots > 0);
    isovalue_geomprop = [-valid_roots, valid_roots];
    
    [~,ia] = sort(isovalue_geomprop(:,1));
    isovalue_geomprop = isovalue_geomprop(ia,:);
    
    
end

end


function cn = calcNewCoeffKnownIsoval(an, geom_prop, known_isovalue)

num_coeff = length(an);
degree = max(roots([1,1,-2*num_coeff]))-1;
cn = zeros(1, degree+1);
[indices, xpowers] = getIndicesAndXPowers(degree+1);
for n = 1:num_coeff
    idx = indices(n);
    cn(idx) = cn(idx) + an(n)*known_isovalue^xpowers(n);
end
cn(1) = cn(1) - geom_prop;

end

function cn = calcNewCoeffSameAbsIsoval(an, geom_prop)

num_coeff = length(an);
degree = max(roots([1,1,-2*num_coeff]))-1;

cn = zeros(1, degree+1);
this_coeff_set = [];
counter = 1;
this_degree = 1;
neg_counter = 2;
this_cn_sign = 2;
while this_degree <= degree+1
    this_sign = (-1)^neg_counter;
    this_coeff_set = [this_coeff_set, this_sign*an(counter)]; %#okAGROW
    counter = counter + 1;
    neg_counter = neg_counter + 1;
    if numel(this_coeff_set) == this_degree
        %         cn_sign = (-1)^(mod(this_cn_sign,2));
        %         cn(this_degree) = cn_sign*sum(this_coeff_set);
        cn(this_degree) = sum(this_coeff_set);
        this_coeff_set = [];
        this_degree = this_degree + 1;
        %         neg_counter = 1;
        this_cn_sign = this_cn_sign + 1;
        %         neg_counter=2;
        neg_counter = this_cn_sign;
    end
    
end
cn(1) = cn(1) - geom_prop;

end

function cn = calcNewCoeffSameAbsIsovalalt(an, geom_prop)

num_coeff = length(an);
degree = max(roots([1,1,-2*num_coeff]))-1;

cn = zeros(1, degree+1);
this_coeff_set = [];
counter = 1;
this_degree = 1;
neg_counter = 2;
this_cn_sign = 2;
while this_degree <= degree+1
    this_sign = (-1)^neg_counter;
    this_coeff_set = [this_coeff_set, this_sign*an(counter)]; %#okAGROW
    counter = counter + 1;
    neg_counter = neg_counter + 1;
    if numel(this_coeff_set) == this_degree
        %         cn_sign = (-1)^(mod(this_cn_sign,2));
        %         cn(this_degree) = cn_sign*sum(this_coeff_set);
        cn(this_degree) = sum(this_coeff_set);
        this_coeff_set = [];
        this_degree = this_degree + 1;
        %         neg_counter = 1;
        this_cn_sign = this_cn_sign + 1;
        neg_counter=2;
        %        neg_counter = this_cn_sign;
    end
    
end
cn(1) = cn(1) - geom_prop;

end



function [indices, xpowers] = getIndicesAndXPowers(degree)

A = [1:degree]';
A_new = repmat(A,1,degree);
reorg_A = flipud(triu(A_new));
xpowers = nonzeros(reorg_A(:))-1;

uA = triu(A_new);
indices = nonzeros(uA(:));

end

function [indices, ypowers] = getIndicesAndYPowers(degree)

A = [1:degree]';
A_new = repmat(A,1,degree);
reorg_A = flipud(triu(A_new));
indices = nonzeros(reorg_A(:));

uA = triu(A_new);
ypowers = nonzeros(uA(:)) - 1;

end
