function [isovalue_geomprop, struct_type] = calculateMainIsovalues(coeffs, struct_type, geom_prop_unitcell)
%Calculate main isovalues based on geometrical specification input

coeffs = char2numcell(coeffs);
    
if struct_type==2 %double surface structure
    fake_arg_in = 'not double';
    isovalue_geomprop = calculateIsovalForGeomProp(coeffs, geom_prop_unitcell, fake_arg_in);
elseif struct_type ==1 %single surface structure
    isovalue_geomprop = calculateIsovalForGeomProp(coeffs, geom_prop_unitcell);
end


end