function testMainIsovalues(main_isovalues, alias)
%check whether main isovalues used are valid for tpms

[~, ~, lattice_range_isovalues, ~] = lookupTPMSHandleAndOffsets(alias);
isvalid = main_isovalues >= lattice_range_isovalues(1) & main_isovalues <= lattice_range_isovalues(end);

if any(~isvalid)
    error('Main isovalues are not within valid lattice range. Geometric specification is not valid.')
end

end