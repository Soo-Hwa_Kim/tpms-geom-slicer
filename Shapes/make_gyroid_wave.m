function result = make_gyroid_wave(z, x, height, width)

if nargin < 3
    height = 2*pi;
    width = 2*pi;
end

M_PI = pi;
M_PI_2 = 2*pi;
%Note here x is an array of points

%Not yet doing scaling, only doing one unit cell!

zsin = round(sin(z), 8);
zcos = round(cos(z), 8);

vertical = zsin <= zcos;
lower_bound = 0;
upper_bound = height;
if vertical
    flip = false;
    lower_bound = -M_PI;
    upper_bound = width - M_PI_2;
    [width, height] = swap(width, height);
else
    flip = true;
end

one_period_odd = make_one_period(x, zcos, zsin, vertical, flip);
flip = ~flip;
one_period_even = make_one_period(x, zcos, zsin, vertical, flip);


itr_range = lower_bound:M_PI:upper_bound;
%Need to loop over bounds later once doing more than a unit cell
result = zeros(2*length(itr_range), size(x,1), 2);
% for now the width and height are just 2PI
% width = 2*pi;
% height = 2*pi;
for y_itr = [0]
counter = 1;
% for y_itr = itr_range
    y0 = y_itr;
    % creates odd polylines
    flip = ~flip;
    try
    result(2*counter-1,:,:) = make_wave(one_period_odd, width, height, y0, zcos, zsin, vertical, flip);    
    catch
        keyboard
    end
    % creates even polylines
    y0 = y0 + M_PI;
    flip = ~flip;
    result(2*counter,:,:) = make_wave(one_period_even, width, height, y0, zcos, zsin, vertical, flip);
    counter = counter + 1;
end



    function polyline = make_wave(one_period, width, height, offset, zcos, zsin, vertical, flip)
        %offset is the y-position of this particular wave.
        
        dx = one_period(2,1) - one_period(1,1);
        points = zeros(ceil(width/dx)+1, 2);
        
        for m = 1:size(points, 1)
            if m == size(points, 1)
                %this is the last point, make sure it is on the boundary
                points(m,:) = [width, fy(width, zsin, zcos, vertical, flip)];
            else
                % This is the index to loop over one period except the last point
                m_mod = mod(m-1, size(one_period, 1)-1) + 1;
                points(m,:) = one_period(m_mod, :);
            end
        end
        
        polyline = zeros(size(points));
        
        for m = 1:size(polyline, 1)
            x_val = points(m,1);
            y_val = points(m,2) + offset;
            %these two lines are the clamp in C
            y_val = max(0, y_val);
            y_val = min(y_val, height);
            
            if vertical
                polyline(m,:) = [y_val, x_val];
            else
                polyline(m,:) = [x_val, y_val];
            end
        end
    end

    function points = make_one_period(x, zcos, zsin, vertical, flip)
        % NOTE! Not doing piecewise increase in resolution up to requested tolerance
        % Only doing for one set of preallocated x_y vals
        y = zeros(length(x), 1);
        for n = 1:length(x)
            y(n) = fy(x(n), zsin, zcos, vertical, flip);
        end
        points = [x, y];
    end
end