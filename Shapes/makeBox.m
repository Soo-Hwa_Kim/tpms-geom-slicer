function box = makeBox(varargin)

if length(varargin) < 2
    start_pt = 100;
elseif length(varargin) < 1
    length_mm = 20;
    start_pt = 100;
else
    length_mm = varargin{1};
    start_pt = varargin{2};
end

x = [start_pt, start_pt, start_pt + length_mm, start_pt + length_mm, start_pt];

y = [start_pt, start_pt + length_mm, start_pt + length_mm, start_pt, start_pt];

box = [x;y];

box = permute(box, [3 2 1]);

end
