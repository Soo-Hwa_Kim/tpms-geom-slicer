function lines = makeTPMSLines(zslice, tpms, isovalues, num_periods)

[tpms_fun, tpms_gradient] = lookupTPMSHandleAndOffsets(tpms);

[X, Y, Z, V] = createMeshes(tpms_fun, num_periods);
% try
    lines = createContourLines(X, Y, Z, V, zslice, isovalues, tpms_fun, tpms_gradient);
% catch
%     keyboard
% end

end

function [X, Y, Z, V] = createMeshes(tpms_fun, num_periods)

mesh_range = linspace(0, num_periods, 100);
[X, Y, Z] = meshgrid(mesh_range, mesh_range, mesh_range);
V = tpms_fun(X, Y, Z);

end

function lines = createContourLines(x, y, z, v, zslice, isovalues, tpms_fun, tpms_gradient)

if numel(isovalues) == 1
    
    isovalues = [isovalues, isovalues + 0.1, isovalues + 0.2];
    fake_isovalue_flag = true;
    
else
    
    fake_isovalue_flag = false;
    
end


method = 'nearest';
[xi, yi, zi] = meshgrid(x(1, :, 1), y(:, 1, 1), zslice);
vi = interp3(x, y, z, v, xi, yi, zi, method);

c = contours(xi(:, :), yi(:, :), vi(:, :), isovalues);

limit = size(c, 2);
j = 1;
i = 1;
while(j < limit)
    curr_isovalue = c(1, j);
    if fake_isovalue_flag && curr_isovalue == isovalues(2)
        break
    end
    npoints = c(2, j);
    nextj = j + npoints + 1;
    try
    xvalues = c(1, j + 1 : j + npoints);
    yvalues = c(2, j + 1 : j + npoints);
%     this_line(1,:,1) = xvalues;
%     this_line(1,:,2) = yvalues;
        vertices(:,1) = xvalues(:);
        vertices(:,2) = yvalues(:);
    catch
        keyboard
    end
    try
    vertices(:,3) = zslice*ones(size(xvalues(:)));
    catch
        keyboard
    end
    
    % Put in a NR vertex correction around here!! There is a contours
    % resolution where it prints 3 layers the same and then goes to the
    % next contour. NR vertex correction will fix this so they are
    % different with small z changes
    corrected_vertices = correctVerticesConstantZ(vertices, tpms_fun, tpms_gradient, curr_isovalue);
    if max(abs(tpms_fun(vertices(:,1),vertices(:,2),vertices(:,3),curr_isovalue))) < max(abs(tpms_fun(corrected_vertices(:,1),corrected_vertices(:,2),corrected_vertices(:,3),curr_isovalue)))
    keyboard
    end
    if corrected_vertices(1,3) ~= zslice
        keyboard
    end
    this_line(1,:,1) = corrected_vertices(:,1)';
    this_line(1,:,2) = corrected_vertices(:,2)';
    lines{i} = this_line;
    
    clear this_line vertices corrected_vertices
    
    i = i + 1;
    j = nextj;
end

end