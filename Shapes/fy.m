function y = fy(x, zsin, zcos, vertical, flip)

M_PI = pi;


% gyroid function, y = f(x,z)
if vertical
    
    if round(zcos,8) < 0
        phase_offset = 2 * M_PI;
    else
        phase_offset = M_PI;
    end


    a = sin(x + phase_offset);
    b = -zcos;
    
    if flip
        flip_phase = M_PI;
    else
        flip_phase = 0;
    end

    
    res = zsin .* cos(x + phase_offset + flip_phase);
    r = sqrt(a.^2 + b.^2);

    y = asin(a./r) + asin(res./r) + M_PI;

else
    
    if round(zsin,8) < 0
        phase_offset = M_PI;
    else
        phase_offset = 0;
    end
    
    
    a = cos(x + phase_offset);
    b = -zsin;
    
    if flip
        flip_phase = 0;
    else
        flip_phase = M_PI;
    end
    
    
    res = zcos .* sin(x + phase_offset + flip_phase);
    r = sqrt(a.^2 + b.^2);
    
    y = asin(a./r) + asin(res./r) + 0.5*M_PI;

    
end
%This is here because asin in C will return NaN not a complex number
if ~isreal(y)
    y=nan;
end

end