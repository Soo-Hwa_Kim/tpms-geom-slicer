function [solid_filler_lines, f_at_z] = createSolidFillerLinesIsoval(isovalue, fhandle, coeff_arr, num_periods, length_mm, z_mm, Z, vertical_flag, start_pt)

filament_thickness_mm = 0.35;
scale_factor = findScaleFactor(num_periods, length_mm);
filament_thickness_unitcell = filament_thickness_mm * scale_factor;
length_unitcell = length_mm * scale_factor;
Z = Z * scale_factor;
z = z_mm * scale_factor;

edge_isovalue = calculateIsovalForGeomProp(coeff_arr, filament_thickness_unitcell/5, isovalue);
edge_isovalue = edge_isovalue(find(edge_isovalue(:,1)>isovalue));

[xmesh, ymesh, zmesh] = makeSolidMesh(filament_thickness_unitcell, length_unitcell, Z);

fvals = fhandle(xmesh, ymesh, zmesh);
[~,~,idx_z] = ind2sub(size(zmesh),find(zmesh == z));
idx_z = idx_z(1);
x_at_z = reshape(xmesh(:,:,idx_z),size(xmesh(:,:,1)));
y_at_z = reshape(ymesh(:,:,idx_z),size(ymesh(:,:,1)));
f_at_z = reshape(fvals(:,:,idx_z),size(fvals(:,:,1)));
% f_at_z = flipud(f_at_z);
fill_sign = sign(isovalue);

if fill_sign == 1 %true is where we want to fill
%     flogical = f_at_z > edge_isovalue;
    flogical = f_at_z > isovalue;
else
    flogical = f_at_z < edge_isovalue;
end

solid_filler_lines = makeSolidLines(x_at_z, y_at_z, flogical, vertical_flag, scale_factor, start_pt);

end

function [xmesh, ymesh, zmesh] = makeSolidMesh(filament_thickness_unitcell, length_unitcell,Z)

num_lines = floor(length_unitcell / filament_thickness_unitcell);
mesh = linspace(0, length_unitcell, num_lines);
[xmesh, ymesh, zmesh] = meshgrid(mesh, mesh, Z);

end

function lines = makeSolidLines(x_at_z, y_at_z, flogical, vertical_flag, scale_factor, start_pt)

lines_idx_counter = 1;
lines = {};
for n = 1:length(flogical) %assumes we are printing in a square bounding box
    if vertical_flag
        this_fline = flogical(:,n);
    else
        this_fline = flogical(n,:);
    end
    
    fill_idx = find(this_fline);
    if ~isempty(fill_idx)
        try
        stop_line_fill_idx = [find(diff(fill_idx) ~= 1), length(fill_idx)];
        catch
            stop_line_fill_idx = [find(diff(fill_idx) ~= 1)', length(fill_idx)];
        end
        start_line_fill_idx = [1, stop_line_fill_idx + 1];
        
        if length(stop_line_fill_idx) ~= length(start_line_fill_idx)
            start_line_fill_idx = start_line_fill_idx(1:end-1);
        end
        
        for m = 1:length(start_line_fill_idx)
             flipme = logical(mod(m,2));
%             flipme = false;
            this_start_idx = fill_idx(start_line_fill_idx(m));
            this_stop_idx = fill_idx(stop_line_fill_idx(m));
            if this_stop_idx-this_start_idx > 2
                if vertical_flag
                    if ~flipme
                        this_line(:,:,1) = x_at_z(this_start_idx:this_stop_idx,n)';
                        this_line(:,:,2) = y_at_z(this_start_idx:this_stop_idx,n)';
                    else
                        this_line(:,:,1) = fliplr(x_at_z(this_start_idx:this_stop_idx,n)');
                        this_line(:,:,2) = fliplr(y_at_z(this_start_idx:this_stop_idx,n)');
                    end
                else
                    if ~flipme
                        this_line(:,:,1) = x_at_z(n,this_start_idx:this_stop_idx)';
                        this_line(:,:,2) = y_at_z(n,this_start_idx:this_stop_idx)';
                    else
                        this_line(:,:,1) = fliplr(x_at_z(n,this_start_idx:this_stop_idx)');
                        this_line(:,:,2) = fliplr(y_at_z(n,this_start_idx:this_stop_idx)');
                    end
                end
                this_line = this_line / scale_factor + start_pt;
                lines{lines_idx_counter} = this_line;
                lines_idx_counter = lines_idx_counter+1;
                clear this_line
            end
        end
        
    end
end

end
