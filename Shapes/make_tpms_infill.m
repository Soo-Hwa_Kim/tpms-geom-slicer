function lines = make_tpms_infill(num_periods, length_mm, tpms, isovalues, start_pt, z_mm)

scale_factor = findScaleFactor(num_periods, length_mm);
z = z_mm * scale_factor;

% Make tpms points for one unit cell then translate and replicate for multiple
% in x-y plane. Then purge dupes and NaNs
points = makeTPMSLines(z, tpms, isovalues, num_periods);
% unique_points = purgeDupesLines(points);
checkNaNs(points);

% scale gyroid to printer and reorder points in lines so printer starts at
% the nearest point
scaled_lines = scalePointsToPrinter(points, scale_factor, start_pt);
lines = findLineStarts(scaled_lines);

end

% function unique_points = purgeDupesLines(points)
% 
% [~,unique_idxs, ~] = unique([points(:,:,1), points(:,:,2)], 'rows');
% unique_points = points(unique_idxs,:,:);
% 
% end

function checkNaNs(lines)

for n = 1:length(lines)
    this_line = lines{n};
    if any(any(isnan(this_line)))
        warning('There are NaNs among us!')
        keyboard
    end
end

% idx_counter = 1;
% for n = 1:size(lines,1)
%     if any(any(isnan(lines(n,:,:))))
%         non_nan_idx1 = find(~isnan(lines(n,:,1)));
%         non_nan_idx2 = find(~isnan(lines(n,:,2)));
%         non_nan_idx = intersect(non_nan_idx1, non_nan_idx2);
%         if ~isempty(non_nan_idx)
%             multi_non_nan_lines = find(diff(non_nan_idx) ~= 1);
%             if any(multi_non_nan_lines)
%                 prev_idx = non_nan_idx(1);
%                 for m = 1:length(multi_non_nan_lines)+1
%                     if m == length(multi_non_nan_lines)+1
%                         prepped_points{idx_counter} = lines(n, prev_idx:non_nan_idx(end), :);
%                         if isempty(prepped_points{idx_counter})
%                             keyboard
%                         end
%                         idx_counter = idx_counter + 1;
%                     else
%                         prepped_points{idx_counter} = lines(n, prev_idx:non_nan_idx(multi_non_nan_lines(m)), :);
%                         if isempty(prepped_points{idx_counter})
%                             keyboard
%                         end
%                         idx_counter = idx_counter + 1;
%                         try
%                             prev_idx = non_nan_idx(multi_non_nan_lines(m)+1);
%                         catch
%                             keyboard
%                         end
%                     end
%                 end
%             else
%                 prepped_points{idx_counter} = lines(n, non_nan_idx, :);
%                 idx_counter = idx_counter + 1;
%             end
%         end
%     else
%         prepped_points{idx_counter} = lines(n, :, :);
%         idx_counter = idx_counter + 1;
%     end
% end
end

function lines = scalePointsToPrinter(prepped_points, scale_factor, start_mm)

lines = cellfun(@(x) x*(1/scale_factor)+start_mm(1), prepped_points,'un',0);

end
