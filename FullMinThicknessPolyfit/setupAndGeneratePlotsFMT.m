function setupAndGeneratePlotsFMT(SURFACE_DATA)
% setupAndGeneratePlots(SURFACE_DATA)
%----------------------------------------------------------------------------
% setupAndGeneratePlots.m is the overhead function for creating plots. The 
% raw data text files plotted to show the volume, surface area, and minimum
% and maximum thicknesses for solid struts and double surfaces. The
% visualization is turned off and the plots are saved into organized output
% directories. Polynomial fitting coefficients are also output into saved
% directories.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   plots                  saved pdf and fig files
%            txt files              saved txt files of fitting coefficients
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------


%% Minimum Thickness


% Double Surface Thickness

% Import data and set up for plotting

double_surface_thickness_raw_data = dlmread(SURFACE_DATA.filenames.raw_data_double_surface_thickness);

double_surface_thickness_raw_data = checkOffsetRange(double_surface_thickness_raw_data);

[thickness_contour_plot_inputs, coeff_thickness_double, ...
    poly_tol_thickness_double, poly_degree_thickness_double, ...
    raw_data_z] = setupContourPlotInputs(double_surface_thickness_raw_data, ...
    'Double Surface Thickness');

% Create plots

plot2DPolyFitAndResiduals(thickness_contour_plot_inputs{1}, ...
    thickness_contour_plot_inputs{2}, raw_data_z, ...
    thickness_contour_plot_inputs{3}, SURFACE_DATA.alias, ...
    'DoubleSurfaceThickness', SURFACE_DATA.resid_dir);

generatePlots(SURFACE_DATA, thickness_contour_plot_inputs, double_surface_thickness_raw_data, 'Double Surface Thickness');


%% Save all polynomial coefficient data


% Minimum Thickness


data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_thickness_double]);
dlmwrite(data_dir, coeff_thickness_double);


%% Save polynomial fitting tolerances and degrees into log file

headers = {'TPMS', 'GEOM_PROP', 'TOL', 'DEG'};
tpms = {convertCharsToStrings(SURFACE_DATA.alias); convertCharsToStrings(SURFACE_DATA.alias); convertCharsToStrings(SURFACE_DATA.alias)};
geom_props = {"MinThickness"};


tol2d = poly_tol_thickness_double;
deg2d = poly_degree_thickness_double;
table2d = table(tpms, geom_props, tol2d, deg2d, 'VariableNames', headers);
data_dir = fullfile([SURFACE_DATA.polyfit_tol_dir, SURFACE_DATA.filenames.tolerance_2D_log_filename]);
if exist(data_dir, 'file')
    previous_table = readtable(data_dir);
    table2d = [previous_table; table2d];
end
writetable(table2d, data_dir);

end