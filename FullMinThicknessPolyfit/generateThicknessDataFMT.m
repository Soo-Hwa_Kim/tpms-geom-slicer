function generateThicknessDataFMT(SURFACE_DATA)
% generateThicknessData(SURFACE_DATA)
%----------------------------------------------------------------------------
% generateThicknessData.m is the overhead function for calculating
% minimum thickness for both strut and double surfaces. The data is later
% saved into a text file (within generateStrutThicknessData.m and
% generateDoubleSurfaceThicknessData.m). This function is only called if
% raw data for minimum thickness does not exist in correct directory.
%
% Inputs:    SURFACE_DATA           struct
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Calculate minimum thickness




% Calculate minimum thickness for double surfaces


    
    generateDoubleSurfaceThicknessDataFMT(SURFACE_DATA);    



end