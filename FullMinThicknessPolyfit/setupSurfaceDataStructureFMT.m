function SURFACE_DATA = setupSurfaceDataStructureFMT(alias)
% SURFACE_DATA = setupSurfaceDataStructure(alias)
%----------------------------------------------------------------------------
% setupSurfaceDataStructure.m creates the SURFACE_DATA structure that
% carries surface information for calculating geometric properties and
% generating plots. This function will setup SURFACE_DATA with the alias,
% output directories, output filenames, and generate data flags
%
% Inputs:    alias                  string - alias for desired surface
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Initialize structure with alias

SURFACE_DATA = struct();

SURFACE_DATA.alias = alias;


%% Save (and create if don't exist) directories for outputs

SURFACE_DATA = createAndStoreDirectoriesFMT(SURFACE_DATA);


%% Save filenames of outputs

SURFACE_DATA = storeOutputFilenamesFMT(SURFACE_DATA);


%% Set flag for true if raw data doesn't exist

if ~exist([SURFACE_DATA.raw_data_dir, SURFACE_DATA.filenames.raw_data_double_surface_thickness], 'file')
    
    SURFACE_DATA.flags.calculate_double_surface_thickness_flag = true;
    
end


end