function SURFACE_DATA = storeOutputFilenamesFMT(SURFACE_DATA)
% SURFACE_DATA = storeOutputFilenames(SURFACE_DATA)
%----------------------------------------------------------------------------
% storeOutputFilenames.m stores the filenames for all saved outputs in the
% SURFACE_DATA structure. The filenames are for all raw data and plots.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: setupSurfaceDataStructure.m
%----------------------------------------------------------------------------

%% Raw data filenames for text files

SURFACE_DATA.filenames.raw_data_double_surface_thickness = [SURFACE_DATA.alias, 'RawDoubleSurfaceThicknessData.txt'];


%% Output plot filenames for figs and pdfs

% figs

SURFACE_DATA.filenames.contour_plot_min_thickness_fig = [SURFACE_DATA.alias, 'ContourMinThickness.fig'];


% pdfs

SURFACE_DATA.filenames.contour_plot_thickness_pdf = [SURFACE_DATA.alias, 'ContourThickness'];

SURFACE_DATA.filenames.contour_plot_min_thickness_pdf = [SURFACE_DATA.alias, 'ContourMinThickness'];



%% Output polynomial fitting coefficients filenames

SURFACE_DATA.filenames.coeff_thickness_double = [SURFACE_DATA.alias, 'CoeffThicknessDoubleFULL.txt'];


%% Output polynomial fitting tolerance log filenames

SURFACE_DATA.filenames.tolerance_2D_log_filename = 'Polyfit2DToleranceLog.txt';

end