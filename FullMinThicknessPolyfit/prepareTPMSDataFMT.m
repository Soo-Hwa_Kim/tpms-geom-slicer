function SURFACE_DATA = prepareTPMSDataFMT(SURFACE_DATA)
% SURFACE_DATA = prepareTPMSData(SURFACE_DATA)
%----------------------------------------------------------------------------
% prepareTPMSData.m prepares TPMS data to calculate geometric properties. 
% This is only called if raw data needs to be generated. SURFACE_DATA is 
% prepared first by obtaining the implicit function handle (fhandle), 
% gradient function handle (ghandle), and the predetermined offset range 
% where the lattice is valid. Then an initial set of vertices and faces 
% for each offset value are calculated and stored in SURFACE_DATA
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Look up TPMS function handle

SURFACE_DATA = lookupTPMSHandleAndOffsets(SURFACE_DATA);


%% Generate initial vertices and initial faces per offset

SURFACE_DATA = getInitialFacesAndVerticesFMT(SURFACE_DATA);


end