function [layerGCode, wipeGCode] = GCodifyLines(lines, z, layer_idx, layer_max, wipeGCode, hold_header_footer)

[extr_layer, speed_layer] = getEFLayer(layer_idx);

layerGCode{1} = getLayerHeader(z, layer_idx, wipeGCode', hold_header_footer);

if layer_idx == layer_max
    last_layer_flag = true;
else
    last_layer_flag = false;
end
prepped_lines = prepareLines(lines);
[layerGCode{2}, wipeGCode] = makeXYGCode(prepped_lines, extr_layer, speed_layer, z, last_layer_flag);
if ~hold_header_footer
    if layer_idx==layer_max
        layerGCode{3} = getLayerFooter(layer_idx, layer_max, z);
    else
        layerGCode{3} = getLayerFooter(layer_idx, layer_max);
    end
end
layerGCode = cat(1, layerGCode{:});

end

function prepped_lines = prepareLines(lines)
anynan = cellfun(@(x) any(any(isnan(x))),lines,'UniformOutput',false);
if any([anynan{:}])
    keyboard
else
    prepped_lines = lines;
end

end

function [XYGCode, wipeGCode] = makeXYGCode(prepped_lines, extr_layer, speed_layer, z, last_layer_flag)
xyd = [];
for n = 1:size(prepped_lines,2)
    lines = prepped_lines{n};
    for m = 1:size(lines,2)
        if n == 1 && m == 1
            XYGCode{m} = ['G1 X', num2str(lines(n,m,1)), ' Y', num2str(lines(n,m,2))];
            XYGCode{m+1} = ['G1 Z',num2str(z)];
            XYGCode{m+2} = 'G1 E0.80000 F2100.000';
            if last_layer_flag
                XYGCode{m+3} = 'M204 S800';
                XYGCode{m+4} = ['G1 F', num2str(speed_layer)];
            else
                XYGCode{m+3} = ['G1 F', num2str(speed_layer)];
            end
            
        elseif m == 1 %new line, same layer
            try
                XYGCode{start_idx+1} = ['G1 Z',num2str(z+0.4)];
                XYGCode{start_idx+2} = ['G1 X', num2str(lines(1,m,1)), ' Y', num2str(lines(1,m,2)), ' F10800.000'];
                XYGCode{start_idx+3} = ['G1 Z',num2str(z)];
                XYGCode{start_idx+4} = 'G1 E0.80000 F2100.000';
                XYGCode{start_idx+5} = ['G1 F', num2str(speed_layer)];
            catch
                keyboard
            end
        else
            dist_travelled = sqrt((lines(1,m,1)-lines(1,m-1,1))^2+(lines(1,m,2)-lines(1,m-1,2))^2);
            extr_amt = extr_layer * dist_travelled;
            XYGCode{start_idx+1} = ['G1 X', num2str(lines(1,m,1)), ' Y', num2str(lines(1,m,2)), ' E', num2str(extr_amt)];
            xyd = [xyd;lines(1,m,1) lines(1,m,2) dist_travelled];
        end
        
        if m == size(lines,2)
            if size(lines,2) > 3
                [wipeGCode, xyd] = makeWipeGCode(xyd);
            else
                wipeGCode = {'G1 E-0.80000 F2100.000', ...
                    ['G1 Z',num2str(z+0.2),' F10800.000']};
                xyd = [];
            end
            if n ~= size(prepped_lines,2)
                XYGCode = [XYGCode, wipeGCode];
            end
        end
        start_idx = length(XYGCode);
    end
end

XYGCode = XYGCode';

end



function layer_header = getLayerHeader(z, layer_idx, wipeGCode, hold_header_footer)
if ~hold_header_footer
    if layer_idx ~= 1
        layer_header = {newline;...
            [';LAYER ', num2str(layer_idx), ', Z:', num2str(z)];...
            'G92 E0.0'};
        layer_header = [layer_header;wipeGCode];
        layer_header2 = {['G1 Z', num2str(z+0.2), ' F10800.000']};
        layer_header = [layer_header;layer_header2];
        
    else
        layer_header = {newline;...
            [';LAYER ', num2str(layer_idx), ', Z:', num2str(z)]};
        layer_header = [layer_header;wipeGCode];
        layer_header2 ={['G1 Z', num2str(z+0.4), ' F10800.000']};
        layer_header = [layer_header;layer_header2];
    end
else
    layer_header = {newline};
    layer_header = [layer_header;wipeGCode];
    layer_header2 = {['G1 Z', num2str(z+0.2), ' F10800.000']};
    layer_header = [layer_header;layer_header2];
end

end

function layer_footer = getLayerFooter(layer_number, total_layers, final_z)

if layer_number == 1 %fan on
    layer_footer = {'M106 S84.15'};
elseif layer_number == 2
    layer_footer = {'M106 S170.85'};
elseif layer_number == 3
    layer_footer = {'M106 S255'};
elseif layer_number == total_layers
    layer_footer = {'G1 E-0.04000 F2100.000';...
        ['G1 Z',num2str(final_z+0.4),' F10800.000'];...
        'M107';...
        newline};
else
    layer_footer = {};
end


end


