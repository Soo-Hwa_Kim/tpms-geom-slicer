function [wipeGCode, empty_array] = makeWipeGCode(xyd)

reverse_xyd = fliplr(xyd');
[wipe_xy, excess_e] = findWipeXY(reverse_xyd);
checkE(wipe_xy, excess_e);
wipeGCode = GCodifyWipe(wipe_xy, excess_e);
empty_array = [];

end

function [wipe_xye, excess_e] = findWipeXY(reverse_xyd)

max_reverse_dist = 3.29;
neg_e_mm = -0.231;
% try
flag_alt_xypath = sum(reverse_xyd(3,1:end-1)) < max_reverse_dist;
% catch
%     keyboard
% end

if ~flag_alt_xypath
    neg_dist = 0;
    counter = 2;
    wipe_xye = [];
    while (1)
        this_neg_e = reverse_xyd(3,counter-1) * neg_e_mm;
        if this_neg_e == 0
            counter = counter + 1;
            continue;
        end
        this_xyd = reverse_xyd(:,counter);
        neg_dist = neg_dist+reverse_xyd(3,counter-1);
        if neg_dist > max_reverse_dist
            remaining_dist = max_reverse_dist - sum(reverse_xyd(3,1:counter-2));
            vector = [reverse_xyd(1,counter)-reverse_xyd(1,counter-1), reverse_xyd(2,counter)-reverse_xyd(2,counter-1)];
            magnitude = calculateVectorMagnitude(vector);
            finalxy = reverse_xyd(1:2,counter-1) + remaining_dist * (vector'/magnitude);
            finald = sqrt((finalxy(1)-reverse_xyd(1,counter-1))^2 + (finalxy(2)-reverse_xyd(2,counter-1))^2);
            finale = finald * neg_e_mm;
            wipe_xye = [wipe_xye, [finalxy; finale]]; %#okAGROW
            break
        end
        wipe_xye = [wipe_xye, [this_xyd(1:2); this_neg_e]]; %#okAGROW
        counter = counter + 1;
    end
    
else
    wipe_xye = reverse_xyd(1:2,2:end);
    wipe_xye(3,:) = reverse_xyd(3,1:end-1) * neg_e_mm;
    
end
excess_e = -0.8 - sum(wipe_xye(3,:));

end

function wipeGCode = GCodifyWipe(wipe_xy, excess_e)
wipeGCode{1} = ';WIPE_START';
wipeGCode{2} = 'G1 F8640.000';

for m = 1:size(wipe_xy, 2)
   wipeGCode{m+2} = ['G1 X', num2str(wipe_xy(1,m)),' Y', ...
       num2str(wipe_xy(2,m)),' E', num2str(wipe_xy(3,m))]; 
end
wipeGCode{m+3} = ';WIPE_END';
wipeGCode{m+4} = ['G1 E', num2str(excess_e), ' F2100.000'];

end

function checkE(wipe_xy, excess_e)

if any(wipe_xy(3,:)>=0) || excess_e >=0
    warning('Wipe E is not negative!!!')
    keyboard
end

if round(sum(wipe_xy(3,:)) + excess_e, 8) ~= -0.8
    warning('Wipe E does not add up to -0.8')
    keyboard
end

end
