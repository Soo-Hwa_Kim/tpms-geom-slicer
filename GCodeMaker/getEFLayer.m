function [extr_layer, speed_layer] = getEFLayer(layer_idx)

if layer_idx == 1
    extr_layer = 0.0313557754281142; %extrusion rate (extr/mm)
    %     speed_layer = 1200;
else
    extr_layer = 0.033850610782853;
    %     if layer_idx < 4 || layer_max-layer_idx < 4
    %         speed_layer = 2700;
    %     else
    %         speed_layer = 1400; % this seems variable in gcode file so went with an approximate slow speed
    %     end
end
speed_layer = 1200;
extr_layer = extr_layer * 1.1;
end