function start_near_lines = findLineStarts(lines)

first_line = lines{1};
endpt_prev_line = [first_line(1,end,1) first_line(1,end,2)];
start_near_lines = {};
start_near_lines{1} = first_line;

for n = 2:length(lines)
    this_line = lines{n};
    try
    first_point = [this_line(1,1,1) this_line(1,1,2)];
    catch
        keyboard
    end
    last_point = [this_line(1,end,1) this_line(1,end,2)];
    first_dist = sqrt((first_point(1)-endpt_prev_line(1))^2 + (first_point(2)-endpt_prev_line(2))^2);
    last_dist = sqrt((last_point(1)-endpt_prev_line(1))^2 + (last_point(2)-endpt_prev_line(2))^2);

    if first_dist <= last_dist
        start_near_lines{n} = this_line;
        endpt_prev_line = last_point;
    else
        start_near_lines{n} = fliplr(this_line);
        endpt_prev_line = first_point;
    end
    
end

end