function makeGCode(shape_fhandle, finputs, height_mm, start_coords, filename, solid_filler_lines)

if height_mm < 2
    height_mm = 2;
    warning('Height is too small so changed to 2 mm!')
end

layer_height = 0.2;
num_layers = height_mm/layer_height;
Z = layer_height*(1:num_layers);
wipeGCode = {};

for n = 1:num_layers
    z = Z(n);
    lines = shape_fhandle(finputs{:}, z);
    for m = 1:size(start_coords,1)
        this_start_coord = start_coords(m,:);
        if ~isempty(solid_filler_lines)
            if ~isempty(solid_filler_lines{n})
                lines = [lines, solid_filler_lines{n}];
            end
        end
        
        translate = this_start_coord - start_coords(1,:);
        translated_lines = translateLines(lines, translate);
        if m == 1
            hold_header_footer = false;
        else
            hold_header_footer = true;
        end
        
        if n == 1
            [lengthx, widthy] = findLayer1Dims(translated_lines, this_start_coord);
            [brimGCode{m}, wipeGCode] = makeBrimGCode(lengthx, widthy, this_start_coord, wipeGCode);
        end
        
        [thisLayerGCode{m}, wipeGCode] = GCodifyLines(translated_lines, z, n, num_layers, wipeGCode, hold_header_footer);
    end
    if n == 1
        layer1GCode = assembleLayer1GCode(brimGCode, thisLayerGCode);
        layerGCode{n} = layer1GCode;
    else
        layerGCode{n} = thisLayerGCode(:);
    end
    
    % DEBUG --- Visualise lines coords and Gcode %%%
%         if n == 1
% %         if any(n == [1, floor(numel(Z)/4), floor(numel(Z)/2)])
%             scale_factor = findScaleFactor(finputs{1}, finputs{2});
%             plotShapeLines(lines, n, scale_factor);
%             plotGCodeLayerLines(layerGCode{n}, n)
        end
end

layerGCode = cat(1,layerGCode{:});
layerGCode = cat(1,layerGCode{:}); %Two layers of cells

createAndOutputGCode(layerGCode, filename);


end

function createAndOutputGCode(layerGCode, filename)


if ~contains(filename, '.gcode')
    shapename = filename;
    filename = [filename, '.gcode'];
else
    idx_dot = find(filename == '.');
    shapename = filename(1:idx_dot-1);
end

AllGCode = createAllGCode(layerGCode, shapename);

checkGCode(AllGCode);

generateGCodeOutputFile(AllGCode, filename);

end


function AllGCode = createAllGCode(layerGCode, shapename)

TEMPLATE = getGCodeTemplate(shapename);
AllGCode{1} = TEMPLATE.HEADER;
AllGCode{2} = layerGCode;
AllGCode{3} = TEMPLATE.FOOTER;
AllGCode = cat(1,AllGCode{:});

end

function checkGCode(AllGCode)

for n = 1:length(AllGCode)
    
    this_line = AllGCode{n};
    if contains(this_line, 'NaN')
        error('GCode contains NaNs. Will not output file!')
    elseif contains(this_line, 'Z-')
        error('GCode contains negative height. Will not output file!')
        %     elseif contains(this_line, 'Z0.0') || contains(this_line, 'Z0.1')
        %         error('GCode contains low height. Will not output file!')
    end
    
end

end

function generateGCodeOutputFile(AllGCode, filename)

output_dir = fullfile([pwd, '\Outputs\', filename]);
fid = fopen(output_dir, 'w');
for n = 1:length(AllGCode)
    fprintf(fid,[AllGCode{n},'\n']);
end
fclose(fid);

end


function [lengthx, widthy] = findLayer1Dims(lines, start_coords)

xs = [];
ys = [];
for n = 1:length(lines)
    this_line = lines{n};
    this_x = this_line(:,:,1);
    this_y = this_line(:,:,2);
    xs = [xs;this_x(:)]; %#okAGROW
    ys = [ys;this_y(:)]; %#okAGROW
end

lengthx(1) = max(xs) - start_coords(1);
lengthx(2) = min(xs) - start_coords(1);
widthy(1) = max(ys) - start_coords(2);
widthy(2) = min(ys) - start_coords(2);

end

function layer1GCode = assembleLayer1GCode(brimGCode, thisLayerGCode)

if size(brimGCode) ~= size(thisLayerGCode)
    error('different sized line and brim cells :(')
end

layer1GCode = reshape(([brimGCode.' thisLayerGCode.'].'),[],1);

end
