function [brim_gcode, brim_wipe] = makeBrimGCode(lengthx, widthy, print_start_coord, wipeGCode)
% Only for a rectangular brim that prints normal/parallel to bed edges that 
% is not exactly on the edge of the bed (at least 0.6 mm away). Brim print 
% winds inwards

circles = makeCornerCircles;

brim_start_coord = [print_start_coord(1)-0.6 + lengthx(2), ...
    print_start_coord(2) + widthy(2)];

brim_coord = makeBrim(brim_start_coord, print_start_coord, lengthx, widthy, circles);

brim_gcode = GCodifyBrim(brim_coord, wipeGCode);

brim_wipe = GCodifyBrimWipe(brim_coord);


end

function circles = makeCornerCircles
% 3 "winds" of brim

circles{1} = makeCircle(0.6); %outermost circle
circles{2} = makeCircle(0.4);
circles{3} = makeCircle(0.2);

end

function circ = makeCircle(radius)
% Centered around [0, 0]

px = linspace(-radius, radius, 17);
rs = radius*ones(size(px));
py = sqrt(rs.^2 - px.^2);
ny = -fliplr(py);
nx = fliplr(px);

circ = [[px, nx(2:end-1)]; [py, ny(2:end-1)]]';

end

function brim_coord = makeBrim(brim_start_coord, print_start_coord, lengthx, widthy, circles)

brim_coord = [];
start_pt = brim_start_coord;
wind_counter = 1;
circ_counter = 1;

while(1)
    wind = mod(wind_counter,4);
    this_circ = circles{circ_counter};
    len_circ = size(this_circ,1);
    this_circ = fixCircLoc(this_circ, print_start_coord, lengthx, widthy, wind);
    switch wind
        case 1
            straight = [start_pt;this_circ(1,:)];
            arc = this_circ(2:len_circ/4, :);
            end_idx = len_circ/4+1;
        case 2
            straight = [start_pt;this_circ(len_circ/4+1,:)];
            arc = this_circ(len_circ/4+2:2*len_circ/4, :);
            end_idx = 2*len_circ/4+1;
            
        case 3
            straight = [start_pt;this_circ(2*len_circ/4+1,:)];
            arc = this_circ(2*len_circ/4+2:3*len_circ/4, :);
            end_idx = 3*len_circ/4+1;
            circ_counter = circ_counter + 1;
        case 0
            straight = [start_pt;this_circ(3*len_circ/4+1,:)];
            arc = this_circ(3*len_circ/4+2:len_circ, :);
            end_idx = 1;
            
    end
    
    brim_coord = [brim_coord;straight;arc];
    start_pt = this_circ(end_idx,:);
    if circ_counter == 4
        break
    end
    wind_counter = wind_counter + 1;
end


end

function this_circ = fixCircLoc(this_circ, print_start_coord, lengthx, widthy, wind)

switch wind
    case 0
        circx = this_circ(:,1) + print_start_coord(1) + lengthx(2);
        circy = this_circ(:,2) + print_start_coord(2) + widthy(2);
    case 1
        circx = this_circ(:,1) + print_start_coord(1) + lengthx(2);
        circy = this_circ(:,2) + print_start_coord(2) + widthy(1);
    case 2
        circx = this_circ(:,1) + print_start_coord(1) + lengthx(1);
        circy = this_circ(:,2) + print_start_coord(2) + widthy(1);
    case 3
        circx = this_circ(:,1) + print_start_coord(1) + lengthx(1);
        circy = this_circ(:,2) + print_start_coord(2) + widthy(2);
end
this_circ = [circx circy];

end

function brim_gcode = GCodifyBrim(brim_coord, wipeGCode)

layer_idx = 1;
[extr_layer, ~] = getEFLayer(layer_idx);

for m = 2:length(brim_coord)
    
    dist_travelled = sqrt((brim_coord(m,1)-brim_coord(m-1,1))^2+(brim_coord(m,2)-brim_coord(m-1,2))^2);
    extr_amt = extr_layer * dist_travelled;
    brim_lines{m-1,1} = ['G1 X', num2str(brim_coord(m,1)), ' Y', num2str(brim_coord(m,2)), ' E', num2str(extr_amt)];
    
end

[brim_header, brim_footer] = brimHeaderFooter(brim_coord, wipeGCode);

layerGCode{1} = brim_header;
layerGCode{2} = brim_lines;
layerGCode{3} = brim_footer;

brim_gcode = cat(1, layerGCode{:});

end

function [brim_header, brim_footer] = brimHeaderFooter(brim_coord, wipeGCode)

brim_header = {newline; ...
    ';START BRIM'; ...
    'G92 E0.0'};
brim_header = [brim_header;wipeGCode.'];

brim_header2 = {'G1 E-0.80000 F2100.000'; ...
    'G1 Z0.4 F10800.000'; ...
    ['G1 X',num2str(brim_coord(1,1)),' Y',num2str(brim_coord(1,2))]; ...
    'G1 Z0.2'; ...
    'G1 E0.80000 F2100.000'; ...
    'M204 S800'; ...
    'G1 F1200'};
brim_header = [brim_header;brim_header2];

brim_footer = {';END BRIM, NOW PRINT OBJECT'};

end

function brim_wipe = GCodifyBrimWipe(brim_coord)

distances = sqrt(diff(brim_coord(:,1)).^2 + diff(brim_coord(:,2)).^2);
xyd = [brim_coord(2:end,:), distances];
[brim_wipe, ~] = makeWipeGCode(xyd);

end